# Contributing guidelines

## Filing issues

If you have a question or experience any problem using scala-utils, please create an [issue](https://gitlab.com/uit-sfb/scala-utils/issues).


## Submitting patches (pull requests)

We'd love to accept your patches!


## Technical details

To learn more about implementation details and guidelines, please visit our [Wiki](https://gitlab.com/uit-sfb/scala-utils/wikis/Contributing).