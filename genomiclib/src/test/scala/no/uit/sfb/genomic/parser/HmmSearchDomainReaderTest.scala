package no.uit.sfb.genomic.parser

import no.uit.sfb.genomic.TestData
import no.uit.sfb.scalautils.common.data.StreamHolder
import org.scalatest.funspec.AnyFunSpec
import org.scalatest.matchers.should.Matchers

class HmmSearchDomainReaderTest extends AnyFunSpec with Matchers {
  describe("HmmSearchDomainReader") {
    it("should parse 'domtblout' output correctly") {
      val stream = TestData.hmmSearchDomtblout
      val recOpt = HmmSearchDomainRecord.aggregateFromStream(
        StreamHolder(stream),
        it =>
          it.find(
            _.targetName == "M01337:38:000000000-AAYMM:1:2113:26170:12805"))
      recOpt.isDefined shouldBe true

      val rec = recOpt.get
      rec.queryName shouldEqual "16s_arch_for"
      rec.envFrom shouldEqual 1
      rec.envTo shouldEqual 301
    }
  }
}
