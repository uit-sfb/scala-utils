package no.uit.sfb.genomic.parser

import java.io._

import no.uit.sfb.genomic.TestData
import no.uit.sfb.scalautils.common.data.StreamHolder
import org.scalatest.funspec.AnyFunSpec
import org.scalatest.matchers.should.Matchers

class FastqTest extends AnyFunSpec with Matchers {
  describe("FastqReader") {
    it("should parse id properly") {
      val FastqReader.idRegex(id1) = "@seq1.1 1:N:18:1"
      val FastqReader.idRegex(id2) = "@seq1.1/1"
      val FastqReader.idRegex(id3) = "@seq1.1 325:456:1 length=36"
      id1 should be("seq1.1")
      id2 should be("seq1.1")
      id3 should be("seq1.1")
    }
  }
  describe(classOf[FastqReader].getName) {
    it("should parse a fastq file correctly") {
      val records = FastqRecord.fromStream(StreamHolder(TestData.muddyFastq))
      val testRecord =
        records.find(_.id == "M01337:10:000000000-A2V5N:1:1101:17329:1983").get
      testRecord.sequence.startsWith("NAATCTTATAGCGCGGTGCTACCGTGGCTG") shouldBe true
      testRecord.qualityScores.startsWith("#>>AABFFFFFFGGGGGGGGGGHGGHGGHG") shouldBe true
    }
  }

  describe(classOf[FastqWriter].getName) {
    it("should write fastq correctly") {
      val os = new ByteArrayOutputStream()
      val fastq = FastqRecord(
        Vector(
          FastqEntry(
            id = "id1",
            sequence = "atgc",
            qualityScores = "####"
          ),
          FastqEntry(
            id = "id2",
            sequence = "atgc",
            qualityScores = "####"
          )
        ))
      fastq.toStream(os, false)
      os.close()
      val result = new String(os.toByteArray, "UTF-8")
      result.trim shouldEqual
        """
          |@id1
          |atgc
          |+
          |####
          |@id2
          |atgc
          |+
          |####
        """.stripMargin.trim
    }
  }
}
