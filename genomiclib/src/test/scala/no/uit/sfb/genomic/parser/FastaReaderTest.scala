package no.uit.sfb.genomic.parser

import java.io.ByteArrayInputStream

import no.uit.sfb.genomic.TestData
import no.uit.sfb.scalautils.common.data.StreamHolder
import org.scalatest.funspec.AnyFunSpec
import org.scalatest.matchers.should.Matchers

class FastaReaderTest extends AnyFunSpec with Matchers {
  describe("FastaReader") {
    it("should parse fasta correctly") {
      val fastaIt =
        FastaRecord.fromStream(StreamHolder(TestData.predictedGenesProtein))
      val fastaList = fastaIt.toList
      fastaList.take(2) shouldBe List(
        FastaEntry(
          "Cellulose_genome_denovo_accurate_c1?mga?gene_1",
          "GLMVGLGETNEEILQVMRDLRSHGVTMLTIGQYLQPSKDHLPVERYVHPDEFNMFQEEAVKMGFEHAACGPLVRSSYHADKQAAGEEVK"),
        FastaEntry(
          "Cellulose_genome_denovo_accurate_c1?mga?gene_2",
          "MNLKSLKINKGLGMKFFLSAPLLFTWLLAFSALATDKHQKPVVIFETTMGNIVIEVNNKQAPKSAKYFLSLIEQGKFNGTSFYRSGSV" +
            "AGKTPQFIEGGLVDKFILKGDITSVKNSGLPILDDFETTSYSKLKHQVATVSMARDILETGHAIPDIFICLDDIPSFDQNGRQKPDSR" +
            "GFPAFAKVIKGMDVVQKISNKERKGETHIKFLQGQILTTPVIILRAYRINIAQ"
        )
      )
      fastaList.length shouldBe 5
    }
    it("should handle empty files correctly") {
      val empty = new ByteArrayInputStream(Array())
      val fastaReader =
        FastaRecord.fromStream(StreamHolder(empty))
      fastaReader.toList shouldEqual List()
    }
  }
}
