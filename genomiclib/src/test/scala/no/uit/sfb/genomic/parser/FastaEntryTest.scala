package no.uit.sfb.genomic.parser

import org.scalatest.funspec.AnyFunSpec
import org.scalatest.matchers.should.Matchers

class FastaEntryTest extends AnyFunSpec with Matchers {
  describe("FastaRecord") {
    it("should decode IDs") {
      val record = FastaEntry("hello world", "atgcatgc")
      record.id.value shouldEqual "hello"

      val record2 = FastaEntry("hello\tworld", "atgc")
      record2.id.value shouldEqual "hello"

      val record3 = FastaEntry("hello", "atgc")
      record3.id.value shouldEqual "hello"
    }
  }
}
