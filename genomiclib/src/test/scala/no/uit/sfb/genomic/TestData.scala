package no.uit.sfb.genomic

import java.io.InputStream

object TestData {
  def streamFor(name: String): InputStream = getClass.getResourceAsStream(name)

  def predictedGenesNucleotide = streamFor("/predicted_genes_nucleotide.fas")
  def predictedGenesProtein = streamFor("/predicted_genes_protein.fas")
  def mgaOut = streamFor("/mga.out")
  def hmmSearchDomtblout = streamFor("/hmmsearch.domtblout.out")
  def muddyFastq = streamFor("/muddy.fastq")
}
