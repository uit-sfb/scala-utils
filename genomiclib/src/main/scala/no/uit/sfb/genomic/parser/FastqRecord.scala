package no.uit.sfb.genomic.parser

import java.io._

import no.uit.sfb.scalautils.common.{HashRobinWriter, RoundRobinWriter}
import no.uit.sfb.scalautils.common.record.rw.{
  RecordReaderIterator,
  RecordReaderLike,
  RecordWriterLike
}
import no.uit.sfb.scalautils.common.record.{RecordBuilderLike, RecordLike}

case class FastqEntry(id: String, sequence: String, qualityScores: String) {
  def subSequence(startCoord: Int, endCoord: Int): String = {
    //We do not use subList because it is extremely slow!!
    //val dna = DNATools.createDNA(sequence)
    //dna.subList(startCoord, endCoord).seqString
    sequence.substring(startCoord - 1, endCoord)
  }

  lazy val validate: Unit = {
    require(id.nonEmpty, "Fastq entry with empty id.")
    require(sequence.length == qualityScores.length, s"Fastq entry @$id has a sequence and quality scores of different lengths.")
  }
}

case class FastqRecord(record: Vector[FastqEntry])
    extends RecordLike[FastqEntry] {
  protected def format(out: BufferedWriter): RecordWriterLike[FastqEntry] = {
    new FastqWriter(out)
  }
}

object FastqRecord extends RecordBuilderLike[FastqEntry] {
  protected def format(in: BufferedReader): RecordReaderIterator[FastqEntry] = {
    new FastqReader(in).toIterator
  }
}

class FastqReader(in: BufferedReader) extends RecordReaderLike[FastqEntry](in) {
  def readEntry(): Option[FastqEntry] = {
    val FastqReader.idRegex(id) = readLine().getOrElse(return None)
    val sequence = readLine().getOrElse(return None)
    val _ = readLine().getOrElse(return None) // +
    val qualityScores = readLine().getOrElse(return None)

    Some(
      FastqEntry(
        id = id,
        sequence = sequence,
        qualityScores = qualityScores
      ))
  }
}

object FastqReader {
  val idRegex = """^@([^\s/]+).*$""".r
}

class FastqWriter(out: Writer) extends RecordWriterLike[FastqEntry](out) {
  def writeEntry(record: FastqEntry): Unit = {
    printer.println("@" + record.id)
    printer.println(record.sequence)
    printer.println("+")
    printer.println(record.qualityScores)
  }
}

class RoundRobinFastqWriter(out: RoundRobinWriter)
    extends no.uit.sfb.genomic.parser.FastqWriter(out) {
  override def writeEntry(record: FastqEntry): Unit = {
    out.doAndLoadNext(super.writeEntry(record))
  }
}

class HashRobinFastqWriter(out: HashRobinWriter)
    extends no.uit.sfb.genomic.parser.FastqWriter(out) {
  override def writeEntry(record: FastqEntry): Unit = {
    val hash = record.id.hashCode()
    out.hashAndDo(hash, super.writeEntry(record))
  }
}
