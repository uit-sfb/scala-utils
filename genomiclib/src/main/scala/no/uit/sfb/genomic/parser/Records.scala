package no.uit.sfb.genomic.parser

import no.uit.sfb.scalautils.common.record.RecordLike

object Records {
  implicit def fromRecord[E](r: RecordLike[E]): Seq[E] = r.record
  implicit def toFastaRecord(s: Seq[FastaEntry]): FastaRecord =
    FastaRecord(s.toVector)
  implicit def toFastqRecord(s: Seq[FastqEntry]): FastqRecord =
    FastqRecord(s.toVector)
  implicit def toHmmSearchLLDomainRecord(s: Vector[HmmSearchDomainEntry]) =
    HmmSearchDomainRecord(s)
  implicit def toMgaRecord(s: Seq[MgaEntry]) =
    MgaRecord(s.toVector)
}
