package no.uit.sfb.genomic.model

case class Location(start: Int, stop: Int, strand: Strand)
