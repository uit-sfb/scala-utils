package no.uit.sfb.genomic.parser

import java.io.{BufferedReader, BufferedWriter}

import no.uit.sfb.scalautils.common.record.rw.{
  RecordReaderIterator,
  RecordReaderLike,
  RecordWriterLike
}
import no.uit.sfb.scalautils.common.record.{RecordBuilderLike, RecordLike}

import scala.annotation.tailrec

case class HmmSearchDomainEntry(
    targetName: String,
    queryName: String,
    envFrom: Int,
    envTo: Int
)

case class HmmSearchDomainRecord(record: Vector[HmmSearchDomainEntry])
    extends RecordLike[HmmSearchDomainEntry] {
  protected def format(
      out: BufferedWriter): RecordWriterLike[HmmSearchDomainEntry] = ???
}

object HmmSearchDomainRecord extends RecordBuilderLike[HmmSearchDomainEntry] {
  protected def format(
      in: BufferedReader): RecordReaderIterator[HmmSearchDomainEntry] = {
    new HmmSearchDomainReader(in).toIterator
  }
}

class HmmSearchDomainReader(in: BufferedReader)
    extends RecordReaderLike[HmmSearchDomainEntry](in) {
  def readEntry(): Option[HmmSearchDomainEntry] = {
    @tailrec
    def skipCommentsAndEmptyLines(): Option[String] = {
      readLine() match {
        case Some(l) if l != "" && !l.startsWith("#") =>
          Some(l)
        case Some(_) => skipCommentsAndEmptyLines()
        case None    => None
      }
    }
    val line = skipCommentsAndEmptyLines().getOrElse(return None)
    val s = line.split("""\s+""")
    Some(
      HmmSearchDomainEntry(
        targetName = s(0),
        queryName = s(3),
        envFrom = s(19).toInt,
        envTo = s(20).toInt
      ))
  }
}
