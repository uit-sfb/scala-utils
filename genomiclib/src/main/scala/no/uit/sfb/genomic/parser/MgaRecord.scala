package no.uit.sfb.genomic.parser

import java.io._

import no.uit.sfb.scalautils.common.record.rw.{
  RecordReaderIterator,
  RecordReaderLikeWithUnread,
  RecordWriterLike
}
import no.uit.sfb.scalautils.common.record.{RecordBuilderLike, RecordLike}

import scala.collection.mutable.ArrayBuffer

case class MgaPredictedGene(geneDenomination: String,
                            startPos: Int,
                            endPos: Int,
                            strand: String,
                            frame: Int,
                            completePartial: String,
                            geneScore: Float,
                            usedModel: String,
                            rbs: Option[Rbs]) {
  require(strand == "+" || strand == "-")
  def extractFromSequence(seq: String): String = {
    strand match {
      case "+" =>
        seq.substring(startPos, endPos)
      case "-" =>
        seq.substring(startPos, endPos).reverse
      case _ =>
        throw new IllegalStateException("illegal strand (should be + or -")
    }
  }
}

case class Rbs(start: Int, end: Int, score: Float)

case class MgaEntry(header: String,
                    gc: Float,
                    rbs: Float,
                    self: String,
                    predictedGenes: List[MgaPredictedGene])
    extends FastaHeaderLike

case class MgaRecord(record: Vector[MgaEntry]) extends RecordLike[MgaEntry] {
  protected def format(out: BufferedWriter): RecordWriterLike[MgaEntry] = ???
}

object MgaRecord extends RecordBuilderLike[MgaEntry] {
  protected def format(in: BufferedReader): RecordReaderIterator[MgaEntry] = {
    new MgaReader(in).toIterator
  }
}

class MgaReader(in: BufferedReader)
    extends RecordReaderLikeWithUnread[MgaEntry](in) {
  import MgaReader._
  def readEntry(): Option[MgaEntry] = {
    def parsePredictedGene(line: String): MgaPredictedGene = {
      val splits = line.split('\t')
      val rbs: Option[Rbs] = {
        val (start, end, score) = (splits(8), splits(9), splits(10))
        if (List(start, end, score).contains("-")) None
        else Some(Rbs(start.toInt, end.toInt, score.toFloat))
      }
      MgaPredictedGene(
        geneDenomination = splits(0),
        startPos = splits(1).toInt,
        endPos = splits(2).toInt,
        strand = splits(3),
        frame = splits(4).toInt,
        completePartial = splits(5),
        geneScore = splits(6).toFloat,
        usedModel = splits(7),
        rbs = rbs
      )
    }
    def loop(): Seq[MgaPredictedGene] = {
      val builder = ArrayBuffer[MgaPredictedGene]()
      while ({ val line = readLine(); line.isDefined && line.get.nonEmpty }) {
        unreadLine()
        getGene() match {
          case Some(gene) =>
            builder += gene
          case None =>
            unreadLine()
            return builder.toSeq
        }
      }
      builder.toSeq
    }

    def getHeader(): Option[String] = {
      readLine().flatMap {
        case gcRbsHeader(_, _) =>
          None
        case selfHeader(_) =>
          None
        case nameHeader(name) =>
          Some(name)
        case _ =>
          None
      }
    }
    def getGcRbs(): Option[(Float, Float)] = {
      readLine().flatMap {
        case gcRbsHeader(gc, rbs) =>
          Some(gc.toFloat, rbs.toFloat)
        case _ =>
          None
      }
    }
    def getSelf(): Option[String] = {
      readLine().flatMap {
        case gcRbsHeader(_, _) =>
          None
        case selfHeader(self) =>
          Some(self)
        case _ =>
          None
      }
    }
    def getGene(): Option[MgaPredictedGene] = {
      readLine().flatMap {
        case nameHeader(_) =>
          None
        case line =>
          Some(parsePredictedGene(line: String))
      }
    }

    getHeader() flatMap { header =>
      getGcRbs() flatMap {
        case (gc, rbs) =>
          getSelf() map { self =>
            MgaEntry(header, gc, rbs, self, loop().toList)
          }
      }
    }
  }
}

object MgaReader {
  val nameHeader = """^# (.+)$""".r
  val gcRbsHeader =
    """^# gc = (-?[\d]*\.?[\d]*), rbs = (-?[\d]*\.?[\d]*)$""".r
  val selfHeader = """^# self: (.+)$""".r
  def apply(in: InputStream): MgaReader =
    new MgaReader(new BufferedReader(new InputStreamReader(in)))
}
