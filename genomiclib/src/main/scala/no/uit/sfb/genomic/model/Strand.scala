package no.uit.sfb.genomic.model

sealed trait Strand
case object Positive extends Strand { override def toString = "+" }
case object Negative extends Strand { override def toString = "-" }

object Strand {
  def apply(strand: String): Strand = strand match {
    case "+" => Positive
    case "-" => Negative
    case _   => throw new IllegalArgumentException("strand must be either + or -")
  }
}
