package no.uit.sfb.genomic.parser

import java.io._

import no.uit.sfb.scalautils.common.record.rw.{
  RecordReaderIterator,
  RecordReaderLikeWithUnread,
  RecordWriterLike
}
import no.uit.sfb.scalautils.common.record.{RecordBuilderLike, RecordLike}

case class BlastFormat6Entry(queryId: String,
                             subjectId: String,
                             percentIdentity: Double,
                             alignmentLength: Int,
                             mismatchCount: Int,
                             gapOpenCount: Int,
                             queryStart: Int,
                             queryEnd: Int,
                             subjectStart: Int,
                             subjectEnd: Int,
                             eValue: Double,
                             bitScore: Double)

case class BlastFormat6Record(record: Vector[BlastFormat6Entry])
    extends RecordLike[BlastFormat6Entry] {
  protected def format(
      out: BufferedWriter): RecordWriterLike[BlastFormat6Entry] = {
    new BlastFormat6Writer(out)
  }
}

object BlastFormat6Record extends RecordBuilderLike[BlastFormat6Entry] {
  protected def format(
      in: BufferedReader): RecordReaderIterator[BlastFormat6Entry] = {
    new BlastFormat6Reader(in).toIterator
  }
}

class BlastFormat6Reader(in: BufferedReader)
    extends RecordReaderLikeWithUnread[BlastFormat6Entry](in) {
  def readEntry(): Option[BlastFormat6Entry] = {
    readLine() map { line =>
      val splits = line.split('\t')
      BlastFormat6Entry(
        queryId = splits(0),
        subjectId = splits(1),
        percentIdentity = splits(2).toDouble,
        alignmentLength = splits(3).toInt,
        mismatchCount = splits(4).toInt,
        gapOpenCount = splits(5).toInt,
        queryStart = splits(6).toInt,
        queryEnd = splits(7).toInt,
        subjectStart = splits(8).toInt,
        subjectEnd = splits(9).toInt,
        eValue = splits(10).toDouble,
        bitScore = splits(11).toDouble
      )
    }
  }
}

class BlastFormat6Writer(out: Writer)
    extends RecordWriterLike[BlastFormat6Entry](out) {
  def writeEntry(record: BlastFormat6Entry): Unit = {
    val seq = Seq(
      record.queryId,
      record.subjectId,
      record.percentIdentity.toString,
      record.alignmentLength.toString,
      record.mismatchCount.toString,
      record.gapOpenCount.toString,
      record.queryStart.toString,
      record.queryEnd.toString,
      record.subjectStart.toString,
      record.subjectEnd.toString,
      record.eValue.toString,
      record.bitScore.toString
    )
    printer.println(seq.mkString("\t"))
  }
}
