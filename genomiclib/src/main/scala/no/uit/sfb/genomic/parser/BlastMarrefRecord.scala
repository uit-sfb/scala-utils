package no.uit.sfb.genomic.parser

import java.io._

import no.uit.sfb.scalautils.common.record.rw.{
  RecordReaderIterator,
  RecordReaderLikeWithUnread,
  RecordWriterLike
}
import no.uit.sfb.scalautils.common.record.{RecordBuilderLike, RecordLike}

/**
  * Blast query of Marref with the following format (-f | --outfmt):
  * --outfmt 6 qseqid sseqid stitle pident length mismatch gapopen qstart qend sstart send evalue bitscore
  */
case class BlastMarrefEntry(queryId: String,
                            subjectId: String,
                            percentIdentity: Double,
                            alignmentLength: Int,
                            mismatchCount: Int,
                            gapOpenCount: Int,
                            queryStart: Int,
                            queryEnd: Int,
                            subjectStart: Int,
                            subjectEnd: Int,
                            eValue: Double,
                            bitScore: Double,
                            mmpId: String,
                            mmpDb: String,
                            fullScientificName: Option[String])

case class BlastMarrefRecord(record: Vector[BlastMarrefEntry])
    extends RecordLike[BlastMarrefEntry] {
  protected def format(
      out: BufferedWriter): RecordWriterLike[BlastMarrefEntry] = {
    new BlastMarrefWriter(out)
  }
}

object BlastMarrefRecord extends RecordBuilderLike[BlastMarrefEntry] {
  protected def format(
      in: BufferedReader): RecordReaderIterator[BlastMarrefEntry] = {
    new BlastMarrefReader(in).toIterator
  }
}

class BlastMarrefReader(in: BufferedReader)
    extends RecordReaderLikeWithUnread[BlastMarrefEntry](in) {
  private val mmpIdPattern = "/\\[mmp_id=([\\w\\d_-]+)\\]/".r
  private val mmpDbPattern = "/\\[mmp_db=(\\S+)\\]/".r
  //private val fullScientificNamePattern = "/\\[full_scientific_name=(\\S+)\\]/".r

  def readEntry(): Option[BlastMarrefEntry] = {
    readLine() map { line =>
      val splits = line.split('\t')
      BlastMarrefEntry(
        queryId = splits(0),
        subjectId = splits(1),
        mmpId = mmpIdPattern.findFirstIn(splits(2)).getOrElse(""),
        mmpDb = mmpDbPattern.findFirstIn(splits(2)).getOrElse(""),
        fullScientificName = None, //fullScientificNamePattern.findFirstIn(splits(2)),
        percentIdentity = splits(3).toDouble,
        alignmentLength = splits(4).toInt,
        mismatchCount = splits(5).toInt,
        gapOpenCount = splits(6).toInt,
        queryStart = splits(7).toInt,
        queryEnd = splits(8).toInt,
        subjectStart = splits(9).toInt,
        subjectEnd = splits(10).toInt,
        eValue = splits(11).toDouble,
        bitScore = splits(12).toDouble
      )
    }
  }
}

class BlastMarrefWriter(out: Writer)
    extends RecordWriterLike[BlastMarrefEntry](out) {
  def writeEntry(record: BlastMarrefEntry): Unit = {
    val seq = Seq(
      record.queryId,
      record.subjectId,
      record.mmpId,
      record.mmpDb,
      record.fullScientificName.toString,
      record.percentIdentity.toString,
      record.alignmentLength.toString,
      record.mismatchCount.toString,
      record.gapOpenCount.toString,
      record.queryStart.toString,
      record.queryEnd.toString,
      record.subjectStart.toString,
      record.subjectEnd.toString,
      record.eValue.toString,
      record.bitScore.toString
    )
    printer.println(seq.mkString("\t"))
  }
}
