package no.uit.sfb.genomic.parser

import java.io._

import no.uit.sfb.genomic.model.{Location, Negative}
import no.uit.sfb.scalautils.common.RoundRobinWriter
import no.uit.sfb.scalautils.common.record.rw.{
  RecordReaderIterator,
  RecordReaderLikeWithUnread,
  RecordWriterLike
}
import no.uit.sfb.scalautils.common.record.{RecordBuilderLike, RecordLike}
import org.biojava.bio.seq.DNATools

class FeatureId(val value: String) extends Serializable

trait FastaHeaderLike {
  def header: String
  private lazy val split = header.split("\\s")
  lazy val id: FeatureId = new FeatureId(split.head)
  lazy val details = (split.tail map {
    _.split("=").toList match {
      case key :: Nil          => (key, "")
      case key :: value :: Nil => (key, value)
      case _ =>
        throw new RuntimeException(
          s"Error parsing FastaEntry header ('$header').")
    }
  }).toMap
}

case class FastaEntry(header: String, sequence: String)
    extends FastaHeaderLike {
  lazy val sequenceLength = sequence.length
  def subSequence(loc: Location): String = {
    //We do not use subList because it is extremely slow!!
    val trimmedSequence = sequence.substring(loc.start - 1, loc.stop)
    if (loc.strand == Negative) {
      val trimmedDna = DNATools.createDNA(trimmedSequence)
      DNATools.reverseComplement(trimmedDna).seqString()
    } else
      trimmedSequence
  }
}

case class FastaRecord(record: Vector[FastaEntry])
    extends RecordLike[FastaEntry] {
  protected def format(out: BufferedWriter): RecordWriterLike[FastaEntry] = {
    new FastaWriter(out)
  }
}

object FastaRecord extends RecordBuilderLike[FastaEntry] {
  protected def format(in: BufferedReader): RecordReaderIterator[FastaEntry] = {
    new RecordReaderIterator(new FastaReader(in))
  }
}

object FastaRecordWithoutSequences extends RecordBuilderLike[FastaEntry] {
  protected def format(in: BufferedReader): RecordReaderIterator[FastaEntry] = {
    new FastaReader(in, true).toIterator
  }
}

class FastaReader(in: BufferedReader, skipSequence: Boolean = false)
    extends RecordReaderLikeWithUnread[FastaEntry](in) {
  def readEntry(): Option[FastaEntry] = {
    val sequenceBuilder = new StringBuilder
    def readLinesUnitHeader(append: Boolean): Unit = {
      while (true) {
        val l = readLine() match {
          case None =>
            return
          case Some(line) if line.startsWith(">") =>
            unreadLine()
            return
          case Some(line) =>
            line
        }
        if (l.nonEmpty && append)
          sequenceBuilder.append(l)
      }
    }
    readLinesUnitHeader(false)
    val header: String = readLine().getOrElse(return None)
    readLinesUnitHeader(!skipSequence)
    Some(FastaEntry(header.drop(1), sequenceBuilder.mkString))
  }
}

class FastaWriter(out: Writer, width: Int = 75)
    extends RecordWriterLike[FastaEntry](out) {
  def writeEntry(record: FastaEntry): Unit = {
    printer.print(">")
    printer.println(record.header)
    record.sequence.grouped(width).foreach(printer.println)
  }
}

class RoundRobinFastaWriter(out: RoundRobinWriter, width: Int = 75)
    extends no.uit.sfb.genomic.parser.FastaWriter(out, width) {
  override def writeEntry(record: FastaEntry): Unit = {
    out.doAndLoadNext(super.writeEntry(record))
  }
}
