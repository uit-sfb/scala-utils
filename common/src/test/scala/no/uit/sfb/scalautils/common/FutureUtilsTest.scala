/*package no.uit.sfb.scalautils.common

import no.uit.sfb.scalautils.common.FutureUtils._
import org.scalatest.funspec.AnyFunSpec
import org.scalatest.matchers.should.Matchers

import scala.concurrent.duration._
import scala.concurrent.ExecutionContext
import scala.util.{Failure, Success, Try}

class FutureUtilsTest extends AnyFunSpec with Matchers {
  implicit val ec = ExecutionContext.global

  describe(classOf[FutureUtilsTest].getName) {
    describe("RetryOnFailure") {
      case object MyException extends Exception("Action failed")
      class C(i: Int) {
        var j = i
        def decr(): Int = {
          j = j - 1
          if (j > 0)
            throw MyException
          j
        }
      }
      it("should execute the action once") {
        val c = new C(-10)
        Retry(c.decr(), 5, 100.millis)
        c.j should be(-11)
      }
      it("should execute the action until true") {
        val c = new C(2)
        Retry(c.decr(), 5, 100.millis)
        c.j should be(0)
      }
      it("should execute the action until true (max tries)") {
        val c = new C(6)
        Retry(c.decr(), 5, 100.millis)
        c.j should be(0)
      }
      it("should execute the action 6 times then fail") {
        val c = new C(10)
        Try {
          Retry(c.decr(), 5, 100.millis)
        } match {
          case Success(_)           => assert(false)
          case Failure(MyException) => c.j should be(4)
          case _                    => assert(false)
        }
      }
      it("should execute the action 1 times then fail") {
        val c = new C(10)
        Try {
          Retry(c.decr(), 0, 100.millis)
        } match {
          case Success(_)           => assert(false)
          case Failure(MyException) => c.j should be(9)
          case _                    => assert(false)
        }
      }
    }
    describe("RetryUntilTrue") {
      class C(i: Int) {
        var j = i
        def decr(): Boolean = {
          j = j - 1
          j <= 0
        }
      }
      it("should execute the action once") {
        val c = new C(-10)
        RetryUntilTrue(c.decr(), 5, 100.millis)
        c.j should be(-11)
      }
      it("should execute the action until true") {
        val c = new C(2)
        RetryUntilTrue(c.decr(), 5, 100.millis)
        c.j should be(0)
      }
      it("should execute the action until true (max tries)") {
        val c = new C(6)
        RetryUntilTrue(c.decr(), 5, 100.millis)
        c.j should be(0)
      }
      it("should execute the action 6 times then fail") {
        val c = new C(10)
        Try {
          RetryUntilTrue(c.decr(), 5, 100.millis)
        } match {
          case Success(_) => assert(false)
          case Failure(_) => c.j should be(4)
        }
      }
      it("should execute the action 1 times then fail") {
        val c = new C(10)
        Try {
          RetryUntilTrue(c.decr(), 0, 100.millis)
        } match {
          case Success(_) => assert(false)
          case Failure(_) => c.j should be(9)
        }
      }
    }
  }
}*/
