package no.uit.sfb.scalautils.common

import com.typesafe.scalalogging.LazyLogging
import org.scalatest.funspec.AnyFunSpec
import org.scalatest.matchers.should.Matchers

import scala.util.Try

class SystemProcessTest extends AnyFunSpec with Matchers with LazyLogging {
  describe(classOf[SystemProcessTest].getName) {
    it("should execute without throwing") {
      SystemProcess("pwd").exec()
    }
    it("should execute and throw") {
      Try {
        SystemProcess("ls doNotExist").exec()
      }.toOption should be(None)
    }
    it("should execute and return correct exit code and stdout") {
      SystemProcess("echo lala").execQ() should be(0, Seq("lala"), Seq())
    }
    it("should execute and return correct exit code and stderr") {
      SystemProcess("ls doNotExist").execQ() should be(
        2,
        Seq(),
        Seq("ls: cannot access 'doNotExist': No such file or directory"))
    }
    it("should execute and return parsed result") {
      SystemProcess("echo lala").execFQ(_.head) should be(Some("lala"))
    }
    it("should execute and return None") {
      SystemProcess("ls doNotExist").execFQ(_.head) should be(None)
    }
    it("should pipe") {
      val p1 = SystemProcess("echo lala")
      val p2 = SystemProcess("tr l v")
      SystemProcess.pipe(p1, p2).execFQ(_.head) should be(Some("vava"))
    }
    it("should input string") {
      val p1 = "lala"
      val p2 = SystemProcess("tr l v")
      SystemProcess.stdin(p1, p2).execFQ(_.head) should be(Some("vava"))
    }
  }
}
