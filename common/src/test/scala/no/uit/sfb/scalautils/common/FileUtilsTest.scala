package no.uit.sfb.scalautils.common

import org.scalatest.funspec.AnyFunSpec
import org.scalatest.matchers.should.Matchers

import java.nio.file.Paths
import sys.process._

class FileUtilsTest extends AnyFunSpec with Matchers {
  describe(classOf[FileUtilsTest].getName) {
    val path = Paths.get("target/test/test_md5.txt")
    FileUtils.createParentDirs(path)
    FileUtils.writeToFile(path, "qwertyuiop")
    it("Should compute md5 properly") {
      val referenceMd5 = s"md5sum $path".!!.split(" ").head
      val computedMd5 = FileUtils.md5(path)
      FileUtils.deleteDirIfExists(path.getParent)
      computedMd5 should be(referenceMd5)
    }
  }
}
