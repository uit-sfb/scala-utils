package no.uit.sfb.scalautils.common

import org.scalatest.funspec.AnyFunSpec
import org.scalatest.matchers.should.Matchers

class LcmTest extends AnyFunSpec with Matchers {
  describe("gcd") {
    it("should be 6") {
      Arithmetic.gcd(30, 42) should be(6)
    }
  }
  describe("lcm") {
    it("should be 210") {
      Arithmetic.lcm(30, 42) should be(210)
    }
    it("should be 30") {
      Arithmetic.lcm(Set(30)) should be(30)
    }
    it("should be 210 (again)") {
      Arithmetic.lcm(Set(30, 42)) should be(210)
    }
    it("should be 840") {
      Arithmetic.lcm(Set(30, 42, 24)) should be(840)
    }
  }
}
