package no.uit.sfb.scalautils.common.concurrency

/**
  * cancel() cancels an asynchronous task
  * `isCanceled` is true when a task has been canceled
  */
trait CancellableLike {
  def isCancelled: Boolean
  def cancel(): Unit
}
