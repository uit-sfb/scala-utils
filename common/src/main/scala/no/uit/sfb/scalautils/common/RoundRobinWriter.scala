package no.uit.sfb.scalautils.common

import java.io.Writer

class RoundRobinWriter(writers: Seq[Writer]) extends Writer {
  protected val nb = writers.size
  protected var idx: Int = 0
  def doAndLoadNext(action: => Unit): Unit = {
    this.synchronized {
      action
      idx = (idx + 1) % nb
    }
  }
  protected def index(): Int = {
    this.synchronized {
      idx
    }
  }
  override def write(chars: Array[Char], i: Int, i1: Int): Unit = {
    writers(index()).write(chars, i, i1)
  }

  override def flush(): Unit = {
    writers foreach { _.flush }
  }

  override def close(): Unit = {
    writers foreach { _.close }
  }
}
