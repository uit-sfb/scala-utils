package no.uit.sfb.scalautils.common.data

import java.nio.file.Path

import no.uit.sfb.scalautils.common.FileUtils

/**
  * FileHolderLike is a placeholder for data stored on disc).
  * The way the data is accessed is via the method consume
  */
trait FileHolderLike {

  /**
    * Consumes the data pointed by this Dataset
    * Note that 'consume' is meant to be idempotent, meaning that this method can be call multiple times and always give the same result (not one time consumable).
    * Note that T could be Future[X] if needed
    */
  final def consume[T](usage: Path => T, tempDir: Path): T = {
    val tempPath = FileUtils.createTempDir(tempDir)
    seed(tempPath)
    try {
      usage(tempPath)
    } finally {
      FileUtils.deleteDirIfExists(tempPath)
    }
  }

  /**
    * hardLinks the data to a desired location
    * Equivalent of newStream for StreamHolderLike. It is made public for convenience purpose.
    */
  def seed(path: Path): Unit
}
