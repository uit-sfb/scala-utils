package no.uit.sfb.scalautils.common

object Arithmetic {
  def gcd(a: Int, b: Int): Int = {
    assert(a != 0)
    assert(b != 0)
    if (a >= b)
      ordered_gcm(a, b)
    else
      ordered_gcm(b, a)
  }
  protected def ordered_gcm(a: Int, b: Int): Int = {
    assert(a >= b)
    val rest = a % b
    if (rest == 0)
      b
    else
      ordered_gcm(b, rest)
  }

  def lcm(a: Int, b: Int): Int = {
    assert(a != 0)
    assert(b != 0)
    a * b / gcd(a, b)
  }

  def lcm(vals: Set[Int]): Int = {
    assert(vals.nonEmpty)
    vals.fold(1) { (acc, v) =>
      lcm(acc, v)
    }
  }
}
