package no.uit.sfb.scalautils.common.record.rw.impl

import java.io.{BufferedReader, BufferedWriter}

import no.uit.sfb.scalautils.common.record.rw.{
  RecordReaderLike,
  RecordWriterLike
}

class LineWriter(out: BufferedWriter) extends RecordWriterLike[String](out) {
  override def writeEntry(record: String): Unit = printer.println(record)
}

class LineReader(in: BufferedReader) extends RecordReaderLike[String](in) {
  override def readEntry(): Option[String] = readLine()
}
