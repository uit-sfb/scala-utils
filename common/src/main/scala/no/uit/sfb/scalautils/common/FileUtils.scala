package no.uit.sfb.scalautils.common

import java.io._
import java.nio.file._
import java.nio.file.attribute.BasicFileAttributes
import java.util
import java.util.zip._

import scala.collection.JavaConverters._
import scala.util.{Failure, Success, Try}
import org.rauschig.jarchivelib.{
  ArchiveFormat,
  ArchiverFactory,
  CompressionType
}

object FileUtils {

  def exists(path: Path, followSymbolicLink: Boolean = true): Boolean = {
    //We use a random method that do throw IOException (in contrary to exists, notExists, isDirectory, isRegularFile, ...)
    val options =
      if (followSymbolicLink)
        Seq()
      else
        Seq(LinkOption.NOFOLLOW_LINKS)
    Try { Files.getLastModifiedTime(path, options: _*) } match {
      case Success(_)                                        => true
      case Failure(e) if e.isInstanceOf[NoSuchFileException] => false
      case Failure(e)                                        => throw e
    }
  }

  /**
    * Exists and is a file
    * Throws NoSuchFileException if not exist
    */
  def isFile(path: Path, followSymbolicLink: Boolean = true): Boolean = {
    //Do not use isRegularFile directly because it would not throw IOException
    val options =
      if (followSymbolicLink)
        Seq()
      else
        Seq(LinkOption.NOFOLLOW_LINKS)
    Files.getAttribute(path, "isRegularFile", options: _*).asInstanceOf[Boolean]
  }

  /**
    * Exists and is a directory
    * Throws NoSuchFileException if not exist
    */
  def isDirectory(path: Path, followSymbolicLink: Boolean = true): Boolean = {
    //Do not use isDirectory directly because it would not throw IOException
    val options =
      if (followSymbolicLink)
        Seq()
      else
        Seq(LinkOption.NOFOLLOW_LINKS)
    Files.getAttribute(path, "isDirectory", options: _*).asInstanceOf[Boolean]
  }

  /**
    * Exists and is a symbolic link
    * Throws NoSuchFileException if not exist
    */
  def isSymbolicLink(path: Path): Boolean = {
    //Do not use isSymbolicLink directly because it would not throw IOException
    Files.getAttribute(path, "isSymbolicLink").asInstanceOf[Boolean]
  }

  /**
    * List all (directories, files) in a directory
    * If `path` is a file, returns itself.
    * Note: In case a broken symlink is found NoSuchFileException is thrown
    */
  def ls(path: Path,
         followSymbolicLinks: Boolean = true): (Seq[Path], Seq[Path]) = {
    if (FileUtils.isFile(path, followSymbolicLinks))
      (Seq(), Seq(path))
    else {
      val ds: Iterable[Path] = Files.newDirectoryStream(path).asScala
      val (directories, files) =
        ds.partition(d => isDirectory(d, followSymbolicLinks))
      directories.toSeq -> files.toSeq
    }
  }

  /**
    * List all files recursively under the provided directory
    * If `path` is a file, returns itself.
    */
  def filesUnder(path: Path, followSymbolicLinks: Boolean = true): Seq[Path] = {
    val (dirs, files) = ls(path, followSymbolicLinks)
    dirs.foldLeft(files) {
      case (acc, dir) => acc ++ filesUnder(dir, followSymbolicLinks)
    }
  }

  def isEmptyDir(path: Path, followSymbolicLinks: Boolean = true): Boolean = {
    val (dirs, files) = ls(path, followSymbolicLinks)
    (dirs ++ files).isEmpty
  }

  /**
    * Create necessary directories (if needed)
    * No effect if the whole path already exists
    */
  def createDirs(path: Path): Unit = {
    Files.createDirectories(path)
  }

  /**
    * Create necessary directories (if needed) of the parent of this path (useful is path is a file)
    */
  def createParentDirs(path: Path): Unit = {
    createDirs(path.getParent)
  }

  /**
    * Create temp directory under 'path' prefixed with 'prefix'
    * Returns the path of the created dir
    * Note: not deleted automatically
    */
  def createTempDir(path: Path, prefix: String = ""): Path = {
    Files.createTempDirectory(path, prefix)
  }

  /**
    * Create new empty file and necessary directories (if needed)
    * Throws FileAlreadyExistsException if already exists
    */
  def createFile(path: Path): Unit = {
    createParentDirs(path)
    Files.createFile(path)
  }

  /**
    * Rename file or directory (empty or not)
    * No action if renaming to the same name
    * Throws NoSuchFileException if not exist
    * Throws FileAlreadyExistsException if already exist
    */
  def rename(path: Path, newFileName: String): Unit = {
    Files.move(path, path.getParent.resolve(newFileName))
  }

  def size(path: Path): Long = {
    Files.size(path)
  }

  /**
    * Delete file (or empty directory)
    * Throws NoSuchFileException if not exist
    * (Throws DirectoryNotEmptyException if non-empty dir)
    */
  def deleteFile(path: Path): Unit = {
    Files.delete(path)
  }

  /**
    * Delete file (or empty directory) if exist
    * (Throws DirectoryNotEmptyException if non-empty dir)
    */
  def deleteFileIfExists(path: Path): Unit = {
    Files.deleteIfExists(path)
  }

  /**
    * Delete directory recursively (or file)
    * Throws NoSuchFileException if not exist
    * If `keepRoot` is set to true, the root directory is kept (=emptying)
    */
  def deleteDir(path: Path, keepRoot: Boolean = false): Unit = {
    Files.walkFileTree(
      path,
      new SimpleFileVisitor[Path] {
        override def visitFile(
            f: Path,
            basicFileAttributes: BasicFileAttributes): FileVisitResult = {
          Files.delete(f)
          FileVisitResult.CONTINUE
        }

        override def postVisitDirectory(d: Path,
                                        e: IOException): FileVisitResult = {
          if (e == null) {
            if (!keepRoot || d != path)
              Files.delete(d)
            FileVisitResult.CONTINUE
          } else {
            // directory iteration failed
            throw e
          }
        }
      }
    )
  }

  /**
    * Delete directory recursively (or file) if exist
    * No action if not exist
    */
  def deleteDirIfExists(path: Path): Unit = {
    if (exists(path))
      deleteDir(path)
  }

  /**
    * Try to symlink directory (or file) and on failure, copy
    */
  def smartCopy(path: Path, to: Path): Unit = {
    Try {
      symLink(path, to)
    } recover {
      case _ => copyDir(path, to)
    }
  }

  /**
    * Moves a file or (empty directory) to a directory. Rename if 'rename' non-empty.
    * Create destination directory and parents if needed
    * Note: Symbolic Links themselves are moved, not the target.
    */
  def moveFileToDir(path: Path, toDir: Path, rename: String = ""): Unit = {
    val newFileName =
      toDir.resolve(if (rename.isEmpty) path.getFileName.toString else rename)
    moveFile(path, toDir.resolve(newFileName))
  }

  /**
    * Moves a file or (empty directory).
    * Create destination directory and parents if needed
    * Note: 'to' includes renaming
    * Note: Symbolic Links themselves are moved, not the target.
    */
  def moveFile(path: Path, to: Path): Unit = {
    createParentDirs(to)
    Files.move(path, to)
  }

  /**
    * Moves a directory recursively (or empty file).
    * Create destination directory and parents if needed
    * Note: 'to' includes renaming
    * Note: Symbolic Links themselves are moved, not the target.
    */
  def moveDir(path: Path, to: Path): Unit = {
    createDirs(to)
    Files.walkFileTree(
      path,
      new SimpleFileVisitor[Path] {
        override def preVisitDirectory(
            dir: Path,
            attrs: BasicFileAttributes): FileVisitResult = {
          val targetDir = to.resolve(path.relativize(dir))
          try Files.createDirectory(targetDir)
          catch {
            case e: FileAlreadyExistsException =>
              if (!Files.isDirectory(targetDir)) throw e
          }
          FileVisitResult.CONTINUE
        }

        override def visitFile(file: Path,
                               attrs: BasicFileAttributes): FileVisitResult = {
          Files.move(file, to.resolve(path.relativize(file)))
          FileVisitResult.CONTINUE
        }
      }
    )
  }

  /**
    * Copy file (or empty directory) to directory.
    * If symlink, content is copied
    */
  def copyFileToDir(path: Path, toDir: Path): Unit = {
    copyFile(path, toDir.resolve(path.getFileName))
  }

  /**
    * Copy file (or empty directory).
    * Note: 'to' includes renaming
    * If symlink, content is copied
    */
  def copyFile(path: Path, to: Path): Unit = {
    createParentDirs(to)
    Files.copy(path, to)
  }

  /**
    * Copy directory recursively (or file).
    * Note: 'to' includes renaming
    * Symlinks' content is copied
    */
  def copyDir(path: Path, to: Path): Unit = {
    createDirs(to)
    Files.walkFileTree(
      path,
      util.EnumSet.of(FileVisitOption.FOLLOW_LINKS),
      Integer.MAX_VALUE,
      new SimpleFileVisitor[Path] {
        override def preVisitDirectory(
            dir: Path,
            attrs: BasicFileAttributes): FileVisitResult = {
          val targetDir = to.resolve(path.relativize(dir))
          try Files.copy(dir, targetDir)
          catch {
            case e: FileAlreadyExistsException =>
              if (!Files.isDirectory(targetDir)) throw e
          }
          FileVisitResult.CONTINUE
        }

        override def visitFile(file: Path,
                               attrs: BasicFileAttributes): FileVisitResult = {
          Files.copy(file, to.resolve(path.relativize(file)))
          FileVisitResult.CONTINUE
        }
      }
    )
  }

  /**
    * Can point to a directory or file and cross filesystem boundaries or span across partitions
    * If the source gets deleted, the data is lost
    * The parent directories are created if needed
    */
  def symLink(source: Path, link: Path): Unit = {
    if (link != source) {
      createParentDirs(link)
      Files.createSymbolicLink(link, source)
    }
  }

  /**
    * Can only point to a file and cannot cross filesystem boundaries or span across partitions
    * The data is still available after the deletion of the source
    * The parent directories are created if needed
    */
  def hardLink(source: Path, link: Path): Unit = {
    if (link != source) {
      createParentDirs(link)
      Files.createLink(link, source)
    }
  }

  /**
    * Throws NoSuchFileException if not found
    */
  def readFile(path: Path): String = {
    Files.readAllLines(path).asScala.mkString("\n")
  }

  /**
    * Writes to a file.
    * Attention: The file must not exist
    * If exists, the file is overwritten
    */
  def writeToFile(path: Path, str: String): Unit = {
    Files.write(path, str.getBytes)
  }

  /**
    * Stream to a file using an InputStream
    * Attention: The file must not exist
    * The InputStream gets properly closed
    */
  def streamToFile(path: Path, is: InputStream): Unit = {
    //We don't need to wrap with a BufferedOutputStream here since Files.copy already copies chunks of 8Kb
    try { Files.copy(is, path) } finally {
      is.close()
    }
  }

  /**
    * Stream from a file using an OutputStream
    * Throws NoSuchFileException if not exist
    * The OutputStream gets properly closed
    */
  def streamFromFile(path: Path, os: OutputStream): Unit = {
    //We don't need to wrap with a BufferedInputStream here since Files.copy already copies chunks of 8Kb
    try {
      Files.copy(path, os)
    } finally {
      os.close()
    }
  }

  /**
    * Unzip stream if zipped, otherwise returns the same stream
    * BufferedInputStream needed as we use reset()
    * Note: This method is NOT idempotent in the case where the stream was zipped as the ZIP header is read
    * (but is idempotent in the case where the stream was not)
    * The code commented out below is to unTar in case the archive is composed only of one file. But it is not guaranty to work at all.
    */
  def unGzipStream(is: BufferedInputStream): InputStream = {
    is.mark(3) //2 bytes are read in the GZIPInputStream constructor
    try {
      val unzipped = new GZIPInputStream(is, 2097152)
      /*val unzipped = new BufferedInputStream(new GZIPInputStream(is))
      //We remove the tar header
      unzipped.mark(513) //We read the first 512 characters
      val buff = Array.fill(512) { 0.toByte }
      if (unzipped.read(buff) < 512 || !buff.contains(0)) {
        unzipped.reset()
      }*/
      unzipped
    } catch {
      case _: ZipException =>
        is.reset()
        is
      case _: EOFException =>
        is.reset()
        is
    }
  }

  /**
    * Destroys the stream!!
    */
  private def isGzipStreamAndDestroy(is: InputStream): Boolean = {
    try {
      new GZIPInputStream(is, 2097152) //We are already ready is in the constructor!
      true
    } catch {
      case _: ZipException => false
      case _: EOFException => false
    } finally {
      is.close()
    }
  }

  def isGzipped(path: Path): Boolean = {
    if (!isFile(path))
      throw new FileNotFoundException()
    else
      isGzipStreamAndDestroy(Files.newInputStream(path))
  }

  /**
    * Zip all provided files
    * relativePath is used to relativize the paths in the zip. Otherwise the whole structure from root would be zipped.
    * No error if zip already exists: it will be overwritten
    */
  protected def zip(files: Seq[Path], out: Path, relativePath: Path): Unit = {
    def customRelativize(p: Path, root: Path): String = { //File.relativize cannot deal with two absolute files!
      p.toString.stripPrefix(root.toString).stripPrefix("/")
    }
    val zip = new ZipOutputStream(Files.newOutputStream(out))

    try {
      files.foreach { path =>
        zip.putNextEntry(new ZipEntry(customRelativize(path, relativePath)))
        Files.copy(path, zip)
        zip.closeEntry()
      }
    } finally {
      zip.close()
    }
  }

  /**
    * Zip all files in dirPath
    * No error if zip already exists: it will be overwritten
    * If deleteOrig = true: delete origin dir (dirPath)
    */
  def zip(dirPath: Path, out: Path, deleteOrig: Boolean = false): Unit = {
    if (!isDirectory(dirPath))
      throw new FileNotFoundException(
        s"$dirPath does not exist or is not a directory")
    zip(filesUnder(dirPath), out, dirPath)
    if (deleteOrig)
      FileUtils.deleteDir(dirPath)
  }

  /**
    * Unzip the provided dir
    * No error if output files exist: they will be overwritten
    * If deleteOrig = true: delete origin zip (zipPath)
    */
  def unzip(zipPath: Path,
            out: Path,
            deleteOrig: Boolean = false,
            waitComplete: Boolean = false): Unit = {
    import scala.collection.JavaConverters._

    def using[T <: { def close() }, U](resource: T)(block: T => U): U = {
      try {
        block(resource)
      } finally {
        if (resource != null) {
          resource.close()
        }
      }
    }

    def unzipImpl(zipPath: Path, outputPath: Path): Unit = {
      using(new ZipFile(zipPath.toFile)) { zipFile =>
        for (entry <- zipFile.entries.asScala) {
          val path = outputPath.resolve(entry.getName)
          if (entry.isDirectory) {
            Files.createDirectories(path)
          } else {
            Files.createDirectories(path.getParent)
            Files.copy(zipFile.getInputStream(entry), path)
          }
        }
      }
    }

    if (!isFile(zipPath))
      throw new FileNotFoundException(
        s"$zipPath does not exist or is not a file")

    unzipImpl(zipPath, out)

    if (deleteOrig)
      deleteDir(zipPath)
  }

  /**
    * Destroys the stream!!
    */
  private def isZipStreamAndDestroy(is: InputStream): Boolean = {
    try {
      new ZipInputStream(is).getNextEntry != null //We are already reading in the constructor!
    } finally {
      is.close()
    }
  }

  def isZipped(path: Path): Boolean = {
    isZipStreamAndDestroy(Files.newInputStream(path))
  }

  def unTgz(src: Path, dst: Path): Unit = {
    val archiver =
      ArchiverFactory.createArchiver(ArchiveFormat.TAR, CompressionType.GZIP)
    archiver.extract(src.toFile, dst.toFile)
  }

  def tgz(src: Path, dst: Path, archiveName: String): Unit = {
    val archiver =
      ArchiverFactory.createArchiver(ArchiveFormat.TAR, CompressionType.GZIP)
    archiver.create(archiveName, dst.toFile, src.toFile)
  }

  import java.security.{MessageDigest, DigestInputStream}
  import java.io.{File, FileInputStream}

  /**
    * Compute a md5 of a file
    */
  // The output of this function should match the output of running "md5 -q <file>"
  def md5(path: Path): String = {
    val buffer = new Array[Byte](8192)
    val md5 = MessageDigest.getInstance("MD5")

    val dis = new DigestInputStream(new FileInputStream(path.toFile), md5)
    try { while (dis.read(buffer) != -1) {} } finally { dis.close() }

    md5.digest.map("%02x".format(_)).mkString
  }
}
