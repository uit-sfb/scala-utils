package no.uit.sfb.scalautils.common

import com.typesafe.scalalogging.LazyLogging

import scala.collection.generic.CanBuildFrom
import scala.concurrent.duration.Duration
import scala.concurrent.{Await, ExecutionContext, Future, blocking}
import scala.util.{Failure, Success, Try}
import scala.concurrent.duration._
import scala.util.control.NonFatal

object FutureUtils extends LazyLogging {

  /**
    * Retries a Future up to 'maxRetries' separated by `period` in case of failure until success or number of retries expired
    * In case an exception occurs and is not in the domain of `recoverableException`, then failure occurs immediately.
    * Setting maxRetries to 0 will result in the action to be tried exactly once.
    * Setting maxRetries < 0 will retry indefinitely
    */
  //TODO: Implement with a loop as tailrec is not possible here!!
  def retry[T](todo: => Future[T],
               maxRetries: Int = 5,
               period: Duration = 10.seconds,
               recoverableException: PartialFunction[Throwable, Throwable] = {
                 case NonFatal(e) => e
               })(implicit ec: ExecutionContext): Future[T] = {
    if (maxRetries == 0)
      todo
    else {
      todo.recoverWith {
        recoverableException andThen { e =>
          logger.info(s"Retrying action due to:", e)
          blocking {
            Thread.sleep(period.toMillis)
            retry(todo, maxRetries - 1, period, recoverableException)
          }
        }
      }
    }
  }

  /**
    * Retries an action as until maxRetries is reached in case of exception defined in recoverableException.
    * Setting maxRetries to 0 will result in the action to be tried exactly once.
    * Setting maxRetries < 0 will retry indefinitely
  **/
  object Retry {
    def apply[T](todo: => T,
                 maxRetries: Int = 10,
                 period: Duration = 10.seconds,
                 recoverableException: PartialFunction[Throwable, Throwable] = {
                   case NonFatal(e) => e
                 })(implicit ec: ExecutionContext): T = {
      Await.result(retry[T](Future {
        todo
      }, maxRetries, period, recoverableException), (maxRetries + 1) * period)
    }
  }

  /**
    * Retries `condition` until true or until maxRetries elapsed
    */
  object RetryUntilTrue {
    def apply(
        condition: => Boolean,
        maxRetries: Int = 10,
        period: Duration = 10.seconds)(implicit ec: ExecutionContext): Unit = {
      Retry[Unit](assert(condition), maxRetries, period, {
        case e if e.isInstanceOf[AssertionError] => e
      })
    }
  }

  /**
    * Convenience to convert Try to Future. Present in the scala library 2.12 as Future.fromTry
    */
  def fromTry[T](result: Try[T]): Future[T] = {
    result match {
      case Success(v) => Future.successful(v)
      case Failure(e) => Future.failed(e)
    }
  }

  /**
    * Sequence a Map[A, Future[B]] -> Future[Map[A, B]]
    */
  def sequenceMap[A, B](m: Map[A, Future[B]])(
      implicit ec: ExecutionContext): Future[Map[A, B]] = {
    Future.sequence(m map {
      case (a, fb) =>
        fb map { b =>
          (a, b)
        }
    }) map { _.toMap }
  }

  /**
    * Same as Future.sequence but only keep the successful futures.
    * If all the futures failed, then the result is Future(List())
    */
  def sequenceSuccessful[A, M[X] <: TraversableOnce[X]](in: M[Future[A]])(
      implicit cbf: CanBuildFrom[M[Future[A]], A, M[A]],
      executor: ExecutionContext): Future[M[A]] = {
    in.foldLeft(Future.successful(cbf(in))) { (fr, fa) ⇒
      (for (r ← fr; a ← fa) yield r += a) fallbackTo fr
    } map (_.result())
  }

  /**
    * Same as sequenceMap but only keep the successful futures.
    */
  def sequenceMapSuccessful[A, B](m: Map[A, Future[B]])(
      implicit ec: ExecutionContext): Future[Map[A, B]] = {
    sequenceSuccessful(m map {
      case (a, fb) =>
        fb map { b =>
          (a, b)
        }
    }) map { _.toMap }
  }
}
