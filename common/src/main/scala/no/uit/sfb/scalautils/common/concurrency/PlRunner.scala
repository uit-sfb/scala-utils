package no.uit.sfb.scalautils.common.concurrency

import scala.concurrent.{ExecutionContext, Future}

/**
  * Same as Runner without parameter (ParameterLess)
  */
class PlRunner[R](todo: => R, isBlocking: Boolean)(
    implicit ec: ExecutionContext)
    extends Runner[Unit, R](Unit => todo, isBlocking)
    with PlRunnerLike[R] {
  def execute(): Future[R] = execute(())
}

object PlRunner {
  def apply[R](todo: => R, isBlocking: Boolean = false)(
      implicit ec: ExecutionContext): PlRunner[R] =
    new PlRunner[R](todo, isBlocking)(ec)
}

/**
  * Same as CancellableRunner without parameter
  */
class CancellablePlRunner[R](todo: => R,
                             isBlocking: Boolean,
                             ec: ExecutionContext)
    extends CancellableRunner[Unit, R](Unit => todo, isBlocking, ec)

object CancellablePlRunner {
  def apply[R](todo: => R, isBlocking: Boolean = false)(
      implicit ec: ExecutionContext): CancellablePlRunner[R] =
    new CancellablePlRunner[R](todo, isBlocking, ec)
}
