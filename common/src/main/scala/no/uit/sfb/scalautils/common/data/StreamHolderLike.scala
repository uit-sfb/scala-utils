package no.uit.sfb.scalautils.common.data

import java.io.InputStream

/**
  * StreamHolderLike is a placeholder for data potentially to big to fit in memory (or even disc).
  * The way the data is accessed is via the method consume which generate a new underlying instance of InputStream used for streaming the data.
  * StreamHolderLike is only concerned about binary data. And data processing/conversion is done by adding a FilterInputStream or (Filter)Reader within the usage argument in the consume method.
  */
trait StreamHolderLike {

  /**
    * Consumes the data pointed by this Dataset
    * Note that 'consume' is meant to be idempotent, meaning that this method can be call multiple times and always give the same result (not one time consumable).
    * Note that T could be Future[X] if needed
    */
  final def consume[T](usage: InputStream => T): T = {
    val stream = newStream()
    try {
      usage(stream)
    } finally {
      stream.close()
    }
  }

  /**
    * Creates a new instance of InputStream which can be used to stream the data
    */
  protected def newStream(): InputStream
}

object StreamHolder {
  def apply(is: InputStream): StreamHolderLike = {
    new StreamHolderLike {
      protected def newStream(): InputStream = is
    }
  }
}
