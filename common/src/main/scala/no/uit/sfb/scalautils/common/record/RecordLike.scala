package no.uit.sfb.scalautils.common.record

import java.io._
import java.nio.file.Path

import com.typesafe.scalalogging.LazyLogging
import no.uit.sfb.scalautils.common.ParallelGZIPOutputStream
import no.uit.sfb.scalautils.common.record.rw.RecordWriterLike

import scala.language.implicitConversions

/**
  * A Record is a collection of Entries of type E
  * To make use of the full power of records, define the implicit conversions Seq[E] <-> Record
  * RecordLike holds serialization capabilities
  */
trait RecordLike[E] extends LazyLogging {
  def record: Seq[E]

  /**
    * Transforms the original Writer into a formatted Writer
    * Don't forget to close after usage!
    */
  protected def format(out: BufferedWriter): RecordWriterLike[E]

  /**
    * Writes the record to file
    * (Use of streams)
    */
  final def toFile(path: Path, gzip: Boolean = false): Unit = {
    logger.debug(s"Writing record to '$path'")
    val out = new FileOutputStream(path.toFile)
    toStream(out, gzip)
  }

  /**
    * Writes the record to an OutputStream
    * (Use of streams)
    */
  final def toStream(out: OutputStream, gzip: Boolean): Unit = {
    val gout =
      if (gzip)
        new ParallelGZIPOutputStream(out)
      else
        out
    val buff = new BufferedWriter(new OutputStreamWriter(gout))
    val writer = format(buff)
    try {
      writer.writeAll(record)
    } finally {
      writer.close()
      buff.close()
    }
  }
}
