package no.uit.sfb.scalautils.common

import java.io.{ByteArrayInputStream, InputStream}
import java.nio.file.Path

import com.typesafe.scalalogging.Logger
import org.slf4j.LoggerFactory

import scala.collection.mutable.ArrayBuffer
import scala.sys.process._

/** System process wrapper
  *
  * @param pb ProcessBuilder to execute
  * @param eval Evaluates process builder. Returns (exit code, stdout, stderr)
  */
case class SystemProcess(
    pb: ProcessBuilder,
    eval: ProcessBuilder => (Int, Seq[String], Seq[String]),
    logger: Logger) {
  protected def execute(
      throws: Boolean = true): (Int, Seq[String], Seq[String]) = {
    logger.info(s"$pb")
    val (ret, out, err) = eval(pb)
    logger.info(s"<< $pb")
    if (ret != 0) {
      val e = new RuntimeException(
        s"System process '${pb.toString}' returned non-zero status code: '$ret'")
      if (throws)
        throw e
      else
        logger.warn("System process failed", e)
    }
    (ret, out, err)
  }

  /**
    * Execute the system process throwing a RuntimeException in case the result is not 0
    */
  def exec(): Unit = {
    execute(true)
  }
  @deprecated("verbose flag not used anymore")
  def exec(verbose: Boolean): Unit = {
    exec()
  }

  /**
    * Execute the system process and return the exit code, stdout and stderr
    * Does not throw
    */
  def execQ(): (Int, Seq[String], Seq[String]) = {
    execute(false)
  }
  @deprecated("verbose flag not used anymore")
  def execQ(verbose: Boolean): (Int, Seq[String], Seq[String]) = {
    execQ()
  }

  /**
    * Execute the system process and return the result of the parsing function applied to stdout.
    * Throws if non-zero status code
    */
  def execF[T](parse: Seq[String] => T): T = {
    val (_, out, _) = execute(true)
    parse(out)
  }
  @deprecated("verbose flag not used anymore")
  def execF[T](parse: Seq[String] => T, verbose: Boolean): T = {
    execF[T](parse)
  }

  /**
    * Execute the system process and return the result of the parsing function applied to stdout.
    * Does not throw
    */
  def execFO[T](parse: Seq[String] => T): Option[T] = {
    val (ret, out, _) = execute(false)
    if (ret != 0)
      None
    else
      Some(parse(out))
  }

  @deprecated("Use execFO instead")
  def execFQ[T](parse: Seq[String] => T): Option[T] = {
    execFO(parse)
  }
  @deprecated("verbose flag not used anymore")
  def execFQ[T](parse: Seq[String] => T, verbose: Boolean): Option[T] = {
    execFO[T](parse)
  }
}

object SystemProcess {

  lazy val defaultLogger = Logger(LoggerFactory.getLogger("$"))

  protected def pl(cmd: String, verbose: Int, logger: Logger)
    : (ProcessLogger, ArrayBuffer[String], ArrayBuffer[String]) = {
    val shortCmd =
      if (verbose <= 0)
        ""
      else
        s"${cmd.take(verbose)}: "
    val stdout = ArrayBuffer[String]()
    val stderr = ArrayBuffer[String]()
    val pl = ProcessLogger(out => {
      if (verbose >= 0)
        logger.info(s"$shortCmd$out")
      stdout += out
    }, err => {
      if (verbose >= 0)
        logger.warn(s"$shortCmd$err")
      stderr += err
    })
    (pl, stdout, stderr)
  }

  /** Convenience method to build a SystemProcess
    *
    * @param cmd Command to execute. In case the command is complex (contains quotes, spaces ...) use from() below
    * @param env Environment variables
    * @param path Path where the process should be run (if None, the current execution path is used)
    * @param stdin If true, uses the stdin of the current process
    * @param verbose If <0 do not print stdout/err, if verbose = x >= 0 prefixes each stdout/err line
    *                with the command being executed trimmed at character x
    * @return System process
    *
    * Note: cmd should not contain | (use pipe() below) nor redirection operators
    */
  def apply(cmd: String,
            env: Map[String, String] = Map(),
            path: Option[Path] = None,
            stdin: Boolean = false,
            verbose: Int = 0,
            logger: Logger = defaultLogger): SystemProcess = {
    val (processLogger, stdout, stderr) = pl(cmd, verbose, logger)
    SystemProcess(
      Process(cmd, cwd = path.map { _.toFile }, extraEnv = env.toSeq: _*),
      pb =>
        (if (stdin) pb.!<(processLogger) else pb.!(processLogger),
         stdout.toSeq,
         stderr.toSeq),
      logger)
  }

  /** Convenience method to build a SystemProcess
    *
    * @param cmd Command to execute. The difference with apply() is that the command is provided as a sequence of arguments
    * @param env Environment variables
    * @param path Path where the process should be run (if None, the current execution path is used)
    * @param stdin If true, uses the stdin of the current process
    * @param verbose If <0 do not print stdout/err, if verbose = x >= 0 prefixes each stdout/err line
    *                with the command being executed trimmed at character x
    * @return System process
    *
    * Note: cmd should not contain | (use pipe() between processes) nor redirection operators
    */
  def from(cmd: Seq[String],
           env: Map[String, String] = Map(),
           path: Option[Path] = None,
           stdin: Boolean = false,
           verbose: Int = 0,
           logger: Logger = defaultLogger): SystemProcess = {
    val (processLogger, stdout, stderr) = pl(cmd.mkString(" "), verbose, logger)
    SystemProcess(
      Process(cmd, cwd = path.map { _.toFile }, extraEnv = env.toSeq: _*),
      pb =>
        (if (stdin) pb.!<(processLogger) else pb.!(processLogger),
         stdout.toSeq,
         stderr.toSeq),
      logger)
  }

  /**
    * Pipe p1 into p2. The exit code, stdout and stdin of execQ and execF are those of p2
    * @param p1
    * @param p2
    * @return
    */
  def pipe(p1: SystemProcess,
           p2: SystemProcess,
           logger: Logger = defaultLogger): SystemProcess = {
    SystemProcess(p1.pb #| p2.pb, p2.eval, logger = logger)
  }

  /**
    * Use the provided string as stdin for p
    * @param str
    * @param p
    * @return
    */
  def stdin(str: String,
            p: SystemProcess,
            logger: Logger = defaultLogger): SystemProcess = {
    streamIn(new ByteArrayInputStream(str.getBytes()), p, logger)
    //Do not close stream as it is closed automatically by #<
  }

  /**
    * Use the provided InputStream as stdin for p
    * @param is InputStream. Note that since it is passed by name it is created and destroyed automatically for each use
    * @param p
    * @return
    */
  def streamIn(is: => InputStream,
               p: SystemProcess,
               logger: Logger = defaultLogger): SystemProcess = {
    SystemProcess(p.pb #< is, p.eval, logger = logger)
    //Do not close stream as it is closed automatically by #<
  }
}
