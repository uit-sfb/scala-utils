package no.uit.sfb.scalautils.common.data

/**
  * An ObjectHolder is a placeholder for an object of type DataType.
  * DataType could be anything from a case class, an Array[Byte] or an Iterable[ofSomething]
  * It is only meant for holding objects small enough to fit in memory.
  * Note that DataType could be a Future[X]
  */
trait ObjectHolderLike[DataType] {

  /**
    *   Return the data
    */
  def get(): DataType
}
