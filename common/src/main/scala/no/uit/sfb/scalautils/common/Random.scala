package no.uit.sfb.scalautils.common

object Random {
  def alphanumeric(length: Int): String = {
    val r = scala.util.Random.alphanumeric
    r.take(length).mkString
  }
}
