package no.uit.sfb.scalautils.common.concurrency

import java.util.concurrent.{Callable, FutureTask}

import com.typesafe.scalalogging.LazyLogging

import scala.concurrent._
import scala.util.Try

/**
  * Runs an action in parallel (using the ExecutionContext).
  * `isCompleted` returns true when the task completed.
  * The cleanUp() function is executed when the task completes
  * Set isBlocking to true if the task comports some I/O or waiting for other monitors
  */
class Daemon(todo: => Unit,
             protected val autoStart: Boolean,
             isBlocking: Boolean)(implicit ec: ExecutionContext)
    extends DaemonLike
    with LazyLogging {
  protected val ft: FutureTask[Unit] = new FutureTask[Unit](
    new Callable[Unit] {
      override def call(): Unit = {
        lazy val act = {
          Try {
            todo
          } recover {
            case e =>
              logger.error(s"Daemon crashed", e)
          }
        }
        if (isBlocking)
          blocking(act)
        else
          act
      }
    }
  ) {
    override def done(): Unit =
      cleanUp() //Executed when `todo` finishes normally or via cancellation
  }

  def start(): Unit = ec.execute(ft)
  protected def cleanUp(): Unit = ()
  //Executed once the task is completed (normally)

  initialize()
}

object Daemon {
  def apply(todo: => Unit,
            autoStart: Boolean = true,
            isBlocking: Boolean = false)(implicit ec: ExecutionContext) =
    new Daemon(todo, autoStart, isBlocking)(ec)
}

/**
  * Runs an action in parallel (using the ExecutionContext) which can be canceled.
  * `isCancelled` returns true when cancelled before the task completed normally.
  * The cleanUp() function is executed when the task completes via cancellation
  */
class CancellableDaemon(
    todo: => Unit,
    autoStart: Boolean,
    isBlocking: Boolean,
    ec: ExecutionContext
) extends Daemon(todo, autoStart, isBlocking)(ec)
    with CancellableLike {
  def cancel(): Unit = ft.cancel(true)
  final def isCancelled: Boolean = ft.isCancelled
}

object CancellableDaemon {
  def apply(todo: => Unit,
            autoStart: Boolean = true,
            isBlocking: Boolean = false)(implicit ec: ExecutionContext) =
    new CancellableDaemon(todo, autoStart, isBlocking, ec)
}
