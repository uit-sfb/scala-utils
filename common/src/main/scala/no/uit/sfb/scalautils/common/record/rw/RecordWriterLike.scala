package no.uit.sfb.scalautils.common.record.rw

import java.io._

/**
  * Writer for a record (serializer)
  * The assumptions are:
  * *   - a character stream
  * *   - where line separator plays a major role
  */
abstract class RecordWriterLike[-T](out: Writer) extends FilterWriter(out) {
  protected val printer = new PrintWriter(out)

  def writeEntry(record: T)

  final def writeAll(coll: TraversableOnce[T]): Unit = {
    coll.foreach(writeEntry)
    close()
  }

  override def close(): Unit = {
    printer.flush() //Just being cautious
    super.close()
  }
}
