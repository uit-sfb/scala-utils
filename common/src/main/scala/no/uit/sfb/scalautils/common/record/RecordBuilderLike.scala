package no.uit.sfb.scalautils.common.record

import java.io._
import java.nio.file.Path

import com.typesafe.scalalogging.LazyLogging
import no.uit.sfb.scalautils.common.FileUtils
import no.uit.sfb.scalautils.common.data.{StreamHolder, StreamHolderLike}
import no.uit.sfb.scalautils.common.record.rw.RecordReaderIterator

/**
  * RecordBuilderLike holds parsing capabilities of RecordLike
  * The aggregateFrom methods give access to an iterator using Streams under the hood, so the resource is not loaded into memory.
  * The loadFrom methods load the resource in memory
  * To make use of the full power of records, define the implicit conversions Seq[E] <-> Record
  */
abstract class RecordBuilderLike[E] extends LazyLogging {

  /**
    * Transforms the original Reader into a formatted Reader AND adds Iterator API
    * Don't forget to close after usage!
    */
  protected def format(in: BufferedReader): RecordReaderIterator[E]

  /**
    * Aggregate a StreamHolder
    * Note: Do not return the iterator (usage = identity or usage = _.map ...) as the iterator is consumed.
    */
  @deprecated("Use aggregateFromStream instead")
  final def iterateFromStream[T](streamHolder: StreamHolderLike,
                                 usage: RecordReaderIterator[E] => T): T = {
    aggregateFromStream(streamHolder, usage)
  }

  /**
    * Aggregate a StreamHolder
    * Note: Do not return the iterator (usage = identity or usage = _.map ...) as the iterator is consumed.
    */
  final def aggregateFromStream[T](streamHolder: StreamHolderLike,
                                   usage: RecordReaderIterator[E] => T,
                                   bufferSize: Int = 2097152): T = {
    streamHolder.consume(is => {
      val reader: RecordReaderIterator[E] = format(
        new BufferedReader(new InputStreamReader(
          FileUtils.unGzipStream(new BufferedInputStream(is, bufferSize)))))
      usage(reader) //If usage does not consume the whole reader, the later will not be closed
    })
  }

  /**
    * Aggregate from file
    * Note: Do not return the iterator (usage = identity or usage = _.map ...) as the iterator is consumed.
    */
  @deprecated("Use aggregateFromFile instead")
  final def iterateFromFile[T](path: Path,
                               usage: RecordReaderIterator[E] => T): T = {
    aggregateFromFile(path, usage)
  }

  /**
    * Aggregate from file
    * Note: Do not return the iterator (usage = identity or usage = _.map ...) as the iterator is consumed.
    */
  final def aggregateFromFile[T](path: Path,
                                 usage: RecordReaderIterator[E] => T,
                                 bufferSize: Int = 2097152): T = {
    logger.debug(s"Iterating record from '$path'")
    val sh = StreamHolder(new FileInputStream(path.toFile))
    aggregateFromStream(sh, usage, bufferSize)
  }

  /**
    * Loads from file into memory
    */
  final def fromFile(path: Path): Seq[E] = {
    logger.debug(s"Loading record from '$path'")
    aggregateFromFile(path, _.readAll())
  }

  /**
    * Loads from InputStream into memory
    */
  final def fromStream(streamHolder: StreamHolderLike): Seq[E] = {
    aggregateFromStream(streamHolder, _.readAll())
  }
}
