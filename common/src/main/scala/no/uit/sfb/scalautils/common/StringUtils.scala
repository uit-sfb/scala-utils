package no.uit.sfb.scalautils.common

object StringUtils {

  /**
    * Prints the string as is if smaller than maxLength
    * Otherwise prints the truncated string terminated by "..."
    * Returns never more than maxLength characters
    */
  def truncateString(s: String, maxLength: Int = 255): String = {
    val termination = "..."
    if (s.length > maxLength) {
      val diff = maxLength - termination.length
      if (diff >= 0)
        s.take(diff) + termination
      else
        termination.take(maxLength)
    } else
      s
  }

  def printBytes(numBytes: Long): String = {
    val TiB = 1024 * 1024 * 1024 * 1024.toDouble
    val GiB = 1024 * 1024 * 1024.toDouble
    val MiB = 1024 * 1024.toDouble
    val KiB = 1024.toDouble
    numBytes match {
      case size if size > TiB => f"${size / TiB}%1.1f TiB"
      case size if size > GiB => f"${size / GiB}%1.1f GiB"
      case size if size > MiB => f"${size / MiB}%1.1f MiB"
      case size if size > KiB => f"${size / KiB}%1.1f KiB"
      case size               => s"$size B"
    }
  }
}
