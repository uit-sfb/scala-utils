import sbt.Keys.libraryDependencies
import sys.process._

lazy val supportedScalaVersions = List("2.13.8")

ThisBuild / scalaVersion := supportedScalaVersions.head
ThisBuild / organization := "no.uit.sfb"
ThisBuild / organizationName := "SfB"

lazy val gitCommit = settingKey[String]("Git SHA")
lazy val gitRefName = settingKey[String]("Git branch or tag")
lazy val isRelease = settingKey[Boolean]("Is release")

ThisBuild / gitCommit := { sys.env.getOrElse("CI_COMMIT_SHA", "unknown")}
ThisBuild / gitRefName := { sys.env.getOrElse("CI_COMMIT_REF_NAME", "git rev-parse --abbrev-ref HEAD".!!) }
ThisBuild / isRelease := { sys.env.getOrElse("CI_COMMIT_REF_PROTECTED", "false").toBoolean && sys.env.getOrElse("CI_COMMIT_TAG", "").nonEmpty}
ThisBuild / version := gitRefName.value //Do not use slug here as it would replace '.' with '-'

ThisBuild / crossScalaVersions := supportedScalaVersions

lazy val baseSettings = Seq(
  libraryDependencies ++= Seq(
    "com.typesafe.scala-logging" %% "scala-logging" % "3.9.4",
    "ch.qos.logback" % "logback-classic" % "1.2.11" % Test,
    "org.scalatest" %% "scalatest" % "3.2.11" % Test,
    "org.scalatest" %% "scalatest-funspec" % "3.2.11" % Test
  ))

lazy val root = (project in file("."))
  .settings(
    name := "scala-utils",
    publish := {},
    publishLocal := {},
    crossScalaVersions := Nil
  )
  .settings(baseSettings)
  .aggregate(common, json, yaml, http, genomiclib)

lazy val common = (project in file("common"))
  .settings(
    name := "scala-utils-common",
    libraryDependencies ++= Seq(
      "org.rauschig" % "jarchivelib" % "1.2.0"
    ),
    //Cannot be defined as ThisBuild
    publishConfiguration := publishConfiguration.value.withOverwrite(! isRelease.value)
  )
  .settings(baseSettings)

lazy val json = (project in file("json"))
  .settings(
    name := "scala-utils-json",
    libraryDependencies += "org.json4s" %% "json4s-jackson" % "4.0.5",
    publishConfiguration := publishConfiguration.value.withOverwrite(! isRelease.value)
  )
  .settings(baseSettings)
  .dependsOn(common)

lazy val yaml = (project in file("yaml"))
  .settings(
    name := "scala-utils-yaml",
    libraryDependencies ++= Seq(
      "com.fasterxml.jackson.dataformat" % "jackson-dataformat-yaml" % "2.13.2",
      "com.fasterxml.jackson.module" %% "jackson-module-scala" % "2.13.2"
    ),
    publishConfiguration := publishConfiguration.value.withOverwrite(! isRelease.value)
  )
  .settings(baseSettings)
  .dependsOn(common)

lazy val http = (project in file("http"))
  .settings(
    name := "scala-utils-http",
    libraryDependencies += "org.scalaj" %% "scalaj-http" % "2.4.2",
    publishConfiguration := publishConfiguration.value.withOverwrite(! isRelease.value)
  )
  .settings(baseSettings)
  .dependsOn(json)

lazy val genomiclib = (project in file("genomiclib"))
  .settings(
    name := "scala-utils-genomiclib",
    publishConfiguration := publishConfiguration.value.withOverwrite(! isRelease.value)
  )
  .settings(
    baseSettings,
    libraryDependencies ++= Seq(
      "org.biojava" % "core" % "1.9.5"
    )
  )
  .dependsOn(common)