package no.uit.sfb.scalautils.json

import org.json4s.{
  FileInput,
  JValue,
  JsonInput,
  ReaderInput,
  StreamInput,
  StringInput
}
import org.json4s.jackson.JsonMethods
import com.fasterxml.jackson.databind.DeserializationFeature.{
  USE_BIG_DECIMAL_FOR_FLOATS,
  USE_BIG_INTEGER_FOR_INTS
}
import collection.JavaConverters._

class JsonMethodsExt extends JsonMethods {
  def parseMulti(in: JsonInput,
                 useBigDecimalForDouble: Boolean = false,
                 useBigIntForLong: Boolean = true): Seq[JValue] = {
    var reader = mapper.reader(classOf[JValue])
    if (useBigDecimalForDouble)
      reader = reader `with` USE_BIG_DECIMAL_FOR_FLOATS
    if (useBigIntForLong) reader = reader `with` USE_BIG_INTEGER_FOR_INTS

    in match {
      case StringInput(s)   => reader.readValues(s).readAll().asScala.toSeq
      case ReaderInput(rdr) => reader.readValues(rdr).readAll().asScala.toSeq
      case StreamInput(stream) =>
        reader.readValues(stream).readAll().asScala.toSeq
      case FileInput(file) => reader.readValues(file).readAll().asScala.toSeq
    }
  }
}
object JsonMethodsExt extends JsonMethodsExt
