package no.uit.sfb.scalautils.json

import java.nio.file.Path

import no.uit.sfb.scalautils.common.{FileUtils, StringUtils}
import org.json4s.{Formats, NoTypeHints, StringInput}
import org.json4s.jackson.Serialization

import scala.util.Try

@deprecated("0.3.3", "Use https://circe.github.io/circe/ instead.")
class Json(formats: Formats) {
  private implicit val fo: Formats = formats

  /**
    * Attention: It is compulsory to specify the type each time and NOT rely on automatic typing!
    */
  def parse[T: Manifest](input: String): T = {
    val t: Try[T] = Try {
      Serialization.read[T](input)
    } recover {
      case e: Throwable =>
        throw new RuntimeException(
          s"Error while parsing type ${implicitly[Manifest[T]].runtimeClass.getName} with input:\n${StringUtils
            .truncateString(input)}",
          e)
    }
    t.get
  }

  /**
    * Parse multiple objects written sequentially instead of using a proper JSON array
    * Attention: It is compulsory to specify the type each time and NOT rely on automatic typing!
    */
  def parseMulti[T <: AnyRef: Manifest](input: String): Seq[T] = {
    val t: Try[Seq[T]] = Try {
      JsonMethodsExt
        .parseMulti(StringInput(input),
                    formats.wantsBigDecimal,
                    formats.wantsBigInt)
        .map { _.extract(formats, implicitly[Manifest[T]]) }
    } recover {
      case e: Throwable =>
        throw new RuntimeException(
          s"Error while parsing type ${implicitly[Manifest[T]].runtimeClass.getName} with input:\n${StringUtils
            .truncateString(input)}",
          e)
    }
    t.get
  }

  /**
    * Attention: It is compulsory to specify the type each time and NOT rely on automatic typing!
    */
  def serialize[T <: AnyRef: Manifest](input: T): String = {
    val t: Try[String] = Try {
      Serialization.write(input)
    } recover {
      case e: Throwable =>
        throw new RuntimeException(
          s"Error while serializing type ${implicitly[Manifest[T]].runtimeClass.getName} with input:\n${StringUtils
            .truncateString(input.toString)}",
          e)
    }
    t.get
  }

  /**
    * Attention: It is compulsory to specify the type each time and NOT rely on automatic typing!
    */
  def prettyPrint[T <: AnyRef: Manifest](input: T): String = {
    val t: Try[String] = Try {
      Serialization.writePretty(input)
    } recover {
      case e: Throwable =>
        throw new RuntimeException(
          s"Error while serializing type ${implicitly[Manifest[T]].runtimeClass.getName} with input:\n${StringUtils
            .truncateString(input.toString)}",
          e)
    }
    t.get
  }

  /**
    * Attention: It is compulsory to specify the type each time and NOT rely on automatic typing!
    */
  def fromFileAs[T: Manifest](path: Path): T = {
    Json.parse[T](FileUtils.readFile(path))
  }

  /**
    * Parse multiple objects written sequentially instead of using a proper JSON array
    * Attention: It is compulsory to specify the type each time and NOT rely on automatic typing!
    */
  def fromFileAsMulti[T <: AnyRef: Manifest](path: Path): Seq[T] = {
    Json.parseMulti[T](FileUtils.readFile(path))
  }

  /**
    * Attention: The file must not exist
    */
  def toFileAs[T <: AnyRef: Manifest](path: Path,
                                      obj: T,
                                      prettyPrint: Boolean = true): Unit = {
    FileUtils.writeToFile(path,
                          if (prettyPrint)
                            Json.prettyPrint[T](obj)
                          else
                            Json.serialize[T](obj))
  }
}

object Json extends Json(Serialization.formats(NoTypeHints))
