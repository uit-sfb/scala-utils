import no.uit.sfb.scalautils.json.Json
import org.scalatest.funspec.AnyFunSpec
import org.scalatest.matchers.should.Matchers

class JsonTest extends AnyFunSpec with Matchers {
  val obj = A(1)
  val obj2 = A(11, ab = B(22, Seq()), aopt = Some("opt"))
  describe(classOf[JsonTest].getName) {
    it("should serialize and parse to original") {
      val ser = Json.serialize[A](obj)
      val parsed = Json.parse[A](ser)
      parsed should be(obj)
    }
    it("should pretty print and parse to original") {
      val ser = Json.prettyPrint[A](obj)
      val parsed = Json.parse[A](ser)
      parsed should be(obj)
    }
    it("should not serialize None") {
      val ser = Json.serialize[A](obj)
      ser.contains("aopt") should be(false)
    }
    it("should parse multiple objects") {
      val ser = Json.prettyPrint[A](obj)
      val ser2 = Json.prettyPrint[A](obj2)
      val parsed = Json.parseMulti[A](ser ++ ser2)
      parsed should be(Seq(obj, obj2))
    }
    it("should use default values when not present in input") {
      val inp = """
        |{
        |  "aa": 222,
        |  "ghost": "wohooooo"
        |}
      """.stripMargin
      Json.parse[A](inp) should be(A(222))
    }
  }
}

case class A(aa: Int, ab: B = B(2), aopt: Option[String] = None)
case class B(ba: Int, bb: Seq[C] = Seq(C(), C("c1")))
case class C(ca: String = "def\"[ault{C]a}-")
