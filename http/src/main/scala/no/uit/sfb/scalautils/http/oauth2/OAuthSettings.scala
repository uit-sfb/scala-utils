package no.uit.sfb.scalautils.http.oauth2

case class OAuthSettings(tokenEndpoint: String,
                         clientId: String,
                         clientSecret: String,
                         scope: Set[String] = Set())
