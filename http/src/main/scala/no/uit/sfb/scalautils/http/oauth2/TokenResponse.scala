package no.uit.sfb.scalautils.http.oauth2

import scala.concurrent.duration.Duration

case class TokenResponse(accessToken: String,
                         tokenType: String,
                         expiresIn: Option[Duration],
                         refreshToken: Option[String])
