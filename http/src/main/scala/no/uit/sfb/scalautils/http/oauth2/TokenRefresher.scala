package no.uit.sfb.scalautils.http.oauth2

import com.typesafe.scalalogging.LazyLogging
import no.uit.sfb.scalautils.http.HttpClient
import scalaj.http.HttpResponse

import scala.concurrent.duration._

trait TokenRefresher {
  def refresh(scope: Set[String]): TokenInfo
}

class ClientCredentialsRefresher(oAuthSettings: OAuthSettings)
    extends TokenRefresher
    with LazyLogging {

  /**
    * Apparently we don't use any refreshToken (which is a timeout-less token which can be used to get up-to-date accessToken even when the oAuth server is down)
    * So when we need an updated token we just perform a standard token request
    */
  override def refresh(scope: Set[String]): TokenInfo = {
    logger.info(s"Refreshing OAuth token")
    val request = HttpClient
      .wrappedRequest(
        oAuthSettings.tokenEndpoint,
        Map(
          "grant_type" -> "client_credentials",
          "scope" -> scope.mkString(" "),
          "client_secret" -> oAuthSettings.clientSecret,
          "client_id" -> oAuthSettings.clientId
        ),
        Map("Content-Type" -> "application/x-www-form-urlencoded")
      )
      .method("POST")

    val response: HttpResponse[TokenResponse] = HttpClient.stdClient.execute(
      request,
      HttpClient.tokenParser,
      "getting OAuth2 token")

    val token = response.body
    TokenInfo(token.accessToken,
              token.expiresIn.getOrElse(1.hour),
              System.currentTimeMillis().millis)
  }
}
