package no.uit.sfb.scalautils.http.oauth2

trait Grant

trait HasScope {
  def scope: Set[String]
}

case class ClientCredentials(clientId: String,
                             clientSecret: String,
                             scope: Set[String] = Set())
    extends Grant
    with HasScope

case class RefreshToken(refreshToken: String, scope: Set[String] = Set())
    extends Grant
    with HasScope
