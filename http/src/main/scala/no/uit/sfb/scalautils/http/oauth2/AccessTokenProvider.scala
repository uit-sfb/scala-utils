package no.uit.sfb.scalautils.http.oauth2

import scala.concurrent.duration._

class AccessTokenProvider(refresher: TokenRefresher,
                          refreshMargin: Duration,
                          scope: Set[String]) {

  private var tokenInfo: Option[TokenInfo] = None

  /**
    * Returns access valid token (updates it if expired)
    */
  def accessToken(): String = synchronized {
    tokenInfo match {
      case Some(info) if !info.hasExpiredIn(refreshMargin) => () //Do nothing
      case _                                               => tokenInfo = Some(refresher.refresh(scope))
    }
    tokenInfo.get.accessToken
  }
}

object AccessTokenProvider {
  def apply(oAuthSettings: OAuthSettings,
            refreshMargin: Duration = 10.minutes): AccessTokenProvider = {
    val refresher = new ClientCredentialsRefresher(oAuthSettings)
    new AccessTokenProvider(refresher, refreshMargin, oAuthSettings.scope)
  }
}
