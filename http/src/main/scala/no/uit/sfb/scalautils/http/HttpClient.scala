package no.uit.sfb.scalautils.http

import java.io._
import java.nio.file.{Files, Path}

import com.typesafe.scalalogging.LazyLogging
import no.uit.sfb.scalautils.http.oauth2.{
  AccessTokenProvider,
  OAuthSettings,
  TokenResponse
}
import no.uit.sfb.scalautils.common.{FileUtils, StringUtils}
import no.uit.sfb.scalautils.json.Json
import scalaj.http.{
  HttpConstants,
  HttpOptions,
  HttpRequest,
  HttpResponse,
  MultiPart,
  Http => scalajHttp
}

import scala.concurrent.duration._
import scala.util.Try

case class HttpIOException(code: Int, recoverable: Boolean, msg: String)
    extends IOException(
      s"Http IO failed (${if (!recoverable) "ir"}recoverable) with code '$code': $msg")

object HttpIOException {
  def apply(code: Int, msg: String) =
    new HttpIOException(code, HttpErrorCode.isTemporaryFailure(code), msg)
}

class HttpClient() extends LazyLogging {
  import HttpClient._

  /**
    * Create the request
    * @param uri Part of the url before the '?'
    * @param params Key-values added after '?' separated with '&'
    * @param headers Key-value headers
    * @param connTimeout Timeout for establishment of a connection
    * @param readTimeout Timeout from when the connection is established and when then data start arriving
    * @return HttpRequest
    *
    * Note the whole url (uri + params) is obtained with request.urlBuilder(request)
    */
  protected def request(uri: String,
                        params: Map[String, String] = Map(),
                        headers: Map[String, String] = Map(),
                        connTimeout: Duration = 1.second,
                        readTimeout: Duration = 5.seconds): HttpRequest = {
    HttpClient.wrappedRequest(uri, params, headers, connTimeout, readTimeout)
  }

  /**
    * HEAD
    * You will not get any error status
    */
  final def head(uri: String,
                 params: Map[String, String] = Map(),
                 headers: Map[String, String] = Map(),
                 followRedirect: Boolean = true,
                 connTimeout: Duration = 1.second): HttpRequest = {
    request(uri, params, headers, connTimeout)
      .method("HEAD")
      .option(HttpOptions
        .followRedirects(followRedirect))
  }

  /**
    * GET
    */
  final def get(uri: String,
                params: Map[String, String] = Map(),
                headers: Map[String, String] = Map(),
                followRedirect: Boolean = true,
                connTimeout: Duration = 1.second,
                readTimeout: Duration = 5.seconds): HttpRequest = {
    request(uri, params, headers, connTimeout, readTimeout)
      .method("GET")
      .option(HttpOptions
        .followRedirects(followRedirect))
  }

  /**
    * PUT
    */
  final def put(uri: String,
                data: String,
                params: Map[String, String] = Map(),
                headers: Map[String, String] = Map(),
                connTimeout: Duration = 1.second,
                readTimeout: Duration = 5.seconds): HttpRequest = {
    request(uri, params, headers, connTimeout, readTimeout)
      .put(data)
  }
  final def putArray(uri: String,
                     data: Array[Byte],
                     params: Map[String, String] = Map(),
                     headers: Map[String, String] = Map(),
                     connTimeout: Duration = 1.second,
                     readTimeout: Duration = 5.seconds): HttpRequest = {
    request(uri, params, headers, connTimeout, readTimeout)
      .put(data)
  }
  final def putMulti(uri: String,
                     params: Map[String, String] = Map(),
                     headers: Map[String, String] = Map(),
                     fileName: String,
                     mime: String,
                     is: InputStream,
                     size: Long,
                     callback: Long => Unit = _ => (),
                     connTimeout: Duration = 1.second,
                     readTimeout: Duration = 5.seconds): HttpRequest = {
    postMulti(uri,
              params,
              headers,
              fileName,
              mime,
              is,
              size,
              callback,
              connTimeout,
              readTimeout)
      .copy(method = "PUT")
  }

  /**
    * POST
    */
  final def post(uri: String,
                 data: String,
                 params: Map[String, String] = Map(),
                 headers: Map[String, String] = Map(),
                 connTimeout: Duration = 1.second,
                 readTimeout: Duration = 5.seconds): HttpRequest = {
    request(uri, params, headers, connTimeout, readTimeout)
      .postData(data)
  }
  final def postArray(uri: String,
                      data: Array[Byte],
                      params: Map[String, String] = Map(),
                      headers: Map[String, String] = Map(),
                      connTimeout: Duration = 1.second,
                      readTimeout: Duration = 5.seconds): HttpRequest = {
    request(uri, params, headers, connTimeout, readTimeout)
      .postData(data)
  }

  final def postMulti(uri: String,
                      params: Map[String, String] = Map(),
                      headers: Map[String, String] = Map(),
                      fileName: String,
                      mime: String,
                      is: InputStream,
                      size: Long,
                      callback: Long => Unit = _ => (),
                      connTimeout: Duration = 1.second,
                      readTimeout: Duration = 5.seconds): HttpRequest = {
    request(uri, params, headers, connTimeout, readTimeout)
      .postMulti(
        MultiPart(
          fileName,
          fileName,
          mime,
          is,
          size,
          callback
        ))
  }

  /**
    * DELETE
    */
  final def delete(uri: String,
                   params: Map[String, String] = Map(),
                   headers: Map[String, String] = Map(),
                   connTimeout: Duration): HttpRequest = {
    request(uri, params, headers, connTimeout)
      .method("DELETE")
  }

  /**
    * Blocking
    * Use resultAs or rawResult unless what you want is to deal with InputStream directly
    * You can find parsers in HttpConstants or in Http below
    * Use '.body' on the result to get the body
    *
    * Note: parser CANNOT be identity or equivalent, because request.execute() closes the stream!
    */
  def execute[T: Manifest](request: HttpRequest,
                           parser: InputStream => T,
                           description: String = "",
                           skipCheck: Boolean = false): HttpResponse[T] = {
    val resp = (Try {
      request.execute[T](parser)
    } recover {
      case e: IOException =>
        logger.warn(
          s"Could not execute Http request for uri '${request.urlBuilder(request)}'",
          e)
        throw e
    }).get
    if (!skipCheck)
      validateResponseCode(resp, request, description)
    resp
  }

  /**
    * Blocking
    * Smarter than using execute with 'HttpConstants.readString'
    * since parse http body as String using server charset or configured charset
    * Use '.body' on the result to get the body
    */
  def rawResult(request: HttpRequest,
                description: String = ""): HttpResponse[String] = {
    val resp = request.asString
    validateResponseCode(resp, request, description)
    resp
  }

  /**
    * Blocking
    * Smarter than using execute with 'HttpClient.jsonParser'
    * since parse http body as String using server charset or configured charset
    * Use '.body' on the result to get the body
    */
  def resultAs[T: Manifest](request: HttpRequest,
                            description: String = ""): HttpResponse[T] = {
    val resp = rawResult(request, description)
    resp.copy(body = Json.parse[T](resp.body))
  }

  /**
    * Download a file to provided path (including destination file name)
    * Blocking
    * Uses streams
    */
  def download(uri: String, path: Path): HttpResponse[Unit] = {
    val req = get(uri)
    execute(
      req,
      toFileParser(path),
      s"downloading '$uri'"
    )
  }

  /** Blocking
    * Uses streams
    */
  def upload(path: Path, uri: String): HttpResponse[String] = {
    val size = FileUtils.size(path)
    val is = Files.newInputStream(path)
    try {
      val req = putMulti(
        uri,
        Map(),
        Map(),
        "file", //Do not change this!
        "text/plain",
        is,
        size, /*{
            log.debug(
              s"Wrote ${PrettyPrint.bytes(lenWritten)} out of ${PrettyPrint.bytes(size)}")
          }*/
        connTimeout = 1.second,
        readTimeout = 5.minutes
      )
      rawResult(req, s"uploading to '$uri'")
    } finally {
      is.close()
    }
  }
}

class OAuthHttpClient(accessTokenProvider: AccessTokenProvider)
    extends HttpClient {
  override protected def request(
      uri: String,
      params: Map[String, String] = Map(),
      headers: Map[String, String] = Map(),
      connTimeout: Duration = 1.second,
      readTimeout: Duration = 5.seconds): HttpRequest = {
    HttpClient
      .wrappedRequest(uri, params, headers, connTimeout, readTimeout)
      .header("Authorization", "Bearer " + accessTokenProvider.accessToken())
  }
}

case class OAUthErrorMsg(error_description: String, error: String)

object HttpClient extends LazyLogging {

  /**
    * Use false in production!
    */
  val trustAllCertificates: Boolean = false

  if (trustAllCertificates)
    logger.error(s"Flag 'trustAllCertificates' is true. Only for debugging!!")

  /**
    * Constructor
    */
  lazy val stdClient = new HttpClient()

  def oAuthClient(oAuthSettings: OAuthSettings) =
    new OAuthHttpClient(AccessTokenProvider(oAuthSettings))

  /**
    * Utilities
    */
  def prettyPrintRequest(request: HttpRequest): String = {
    s"\nHttp ${request.method} to ${request
      .urlBuilder(request)}\n${request.headers.mkString("\n")}"
  }

  /**
    * Do not use me directly unless you know what you are doing.
    * Use stdClient or oAuthClient instead
    */
  def wrappedRequest(uri: String,
                     params: Map[String, String] = Map(),
                     headers: Map[String, String] = Map(),
                     connTimeout: Duration = 1.second,
                     readTimeout: Duration = 5.seconds): HttpRequest = {
    val req: HttpRequest = scalajHttp(uri)
      .params(params)
      .timeout(connTimeout.toMillis.toInt, readTimeout.toMillis.toInt)
      .headers(headers)
    if (trustAllCertificates) {
      logger.debug(s"Flag 'trustAllCertificates' is true. Only for debugging!!")
      req.option(HttpOptions.allowUnsafeSSL)
    } else req
  }

  def validateResponseCode(response: HttpResponse[_],
                           request: HttpRequest,
                           description: => String): Unit = {
    val code = response.code
    if (HttpErrorCode.isFailure(code)) {
      val msg =
        s"Invalid response code (${HttpErrorCode(code)}) when $description${prettyPrintRequest(request)}"
      logger.warn(msg)
      logger.warn(
        s"Response: ${StringUtils.truncateString(response.body.toString)}")
      throw HttpIOException(code, msg)
    } else {
      logger.info(s"Request: ${prettyPrintRequest(request)}")
      logger.debug(
        s"Response: ${StringUtils.truncateString(response.body.toString)}")
    }
  }

  /********* Parsers *********/
  //Cf HttpConstants for more parsers
  //You will not get any error message if any
  def ignoreParser(in: InputStream): Unit = ()

  //Use this one if you want to be able to get an error message
  def minimumParser(in: InputStream): String = {
    HttpConstants.readString(in)
  }

  def jsonParser[T: Manifest](in: InputStream): T = {
    Json.parse[T](HttpConstants.readString(in))
  }

  def toFileParser(path: Path)(in: InputStream): Unit = {
    FileUtils.streamToFile(path, in)
  }

  def tokenParser(in: InputStream): TokenResponse = {
    case class OAuth2TokenParsing(access_token: String,
                                  token_type: String,
                                  expires_in: Option[String],
                                  refresh_token: Option[String]) {
      val toTokenResponse =
        TokenResponse(access_token, token_type, expires_in.map {
          _.toLong.seconds
        }, refresh_token)
    }
    val resp = HttpConstants.readString(in)
    Try {
      Json.parse[OAuth2TokenParsing](resp).toTokenResponse
    } getOrElse {
      val error = Json.parse[OAUthErrorMsg](resp)
      throw new RuntimeException(
        s"Could not refresh token due to `${error.error_description}`")
    }
  }
}
