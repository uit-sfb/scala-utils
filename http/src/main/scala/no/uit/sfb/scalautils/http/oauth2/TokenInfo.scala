package no.uit.sfb.scalautils.http.oauth2

import scala.concurrent.duration._

case class TokenInfo(accessToken: String,
                     expiresIn: Duration,
                     lastUpdated: Duration) {
  def hasExpired: Boolean = hasExpiredIn(0.second)
  def hasExpiredIn(margin: Duration): Boolean =
    System.currentTimeMillis().millis > (lastUpdated + expiresIn - margin)
}
