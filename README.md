# scala-utils

Utilities for Scala

## Usage

Add the following to your `build.sbt`:
```
resolvers += "gitlab" at "https://gitlab.com/api/v4/groups/2067095/-/packages/maven"
libraryDependencies += "no.uit.sfb" %% "scala-utils-<library>" % "<version>"
```

where `<library>` is one of:
- `common`
- `http`
- `json`
- `yaml`
- `genomiclibs`

Please visit our [Wiki](https://gitlab.com/uit-sfb/scala-utils/wikis/home) for more information about this project.

## Coverage

| Project        | Coverage      |
| -------------- |:-------------:|
| common         | ![common_coverage](https://gitlab.com/uit-sfb/scala-utils/badges/master/coverage.svg?job=coverage_common)      |
| genomiclib     | ![genomiclib_coverage](https://gitlab.com/uit-sfb/scala-utils/badges/master/coverage.svg?job=coverage_genomiclib)      |
| http           | ![http_coverage](https://gitlab.com/uit-sfb/scala-utils/badges/master/coverage.svg?job=coverage_http)      |
| json           | ![json_coverage](https://gitlab.com/uit-sfb/scala-utils/badges/master/coverage.svg?job=coverage_json)      |
| yaml           | ![yaml_coverage](https://gitlab.com/uit-sfb/scala-utils/badges/master/coverage.svg?job=coverage_yaml)      |
