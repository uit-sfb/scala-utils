package no.uit.sfb.scalautils.yaml

import java.nio.file.Path

import com.fasterxml.jackson.databind.{DeserializationFeature, ObjectMapper}
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory
import com.fasterxml.jackson.module.scala.DefaultScalaModule
import no.uit.sfb.scalautils.common.StringUtils

import scala.util.Try

class Yaml(mapper: ObjectMapper) {

  /**
    * Attention: It is compulsory to specify the type each time and NOT rely on automatic typing!
    * Note: In case your case classes have a default value, you MUST define a default constructor (otherwise any missing parameter is parsed as `null`)
    * Ex:
    * case class A(x: Int, y: String = "") {
    *   @JsonCreator
    *   def this() = this(0)
    * }
    * Note that this would make 0 default value for x as well...
    */
  def parse[T: Manifest](input: String): T = {
    val t: Try[T] = Try {
      mapper
        .readValue(input, manifest[T].runtimeClass)
        .asInstanceOf[T]
    } recover {
      case e: Throwable =>
        throw new RuntimeException(
          s"Error while parsing type ${manifest[T].runtimeClass.getName} with input:\n${StringUtils
            .truncateString(input)}",
          e)
    }
    t.get
  }

  /**
    * Attention: It is compulsory to specify the type each time and NOT rely on automatic typing!
    */
  def serialize[T <: AnyRef: Manifest](input: T): String = {
    val t: Try[String] = Try {
      mapper.writeValueAsString(input)
    } recover {
      case e: Throwable =>
        throw new RuntimeException(
          s"Error while serializing type ${manifest[T].runtimeClass.getName} with input:\n${StringUtils
            .truncateString(input.toString)}",
          e)
    }
    t.get
  }

  /**
    * Attention: It is compulsory to specify the type each time and NOT rely on automatic typing!
    */
  def prettyPrint[T <: AnyRef: Manifest](input: T): String = {
    serialize[T](input)
  }

  /**
    * Attention: It is compulsory to specify the type each time and NOT rely on automatic typing!
    */
  def fromFileAs[T: Manifest](path: Path): T = {
    mapper.readValue(path.toFile, manifest[T].runtimeClass).asInstanceOf[T]
  }

  /**
    * Attention: The file must not exist
    */
  def toFileAs[T <: AnyRef: Manifest](path: Path, obj: T): Unit = {
    mapper.writeValue(path.toFile, obj)
  }
}

object Yaml
    extends Yaml(
      new ObjectMapper(new YAMLFactory())
        .registerModule(DefaultScalaModule)
        .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
    )
