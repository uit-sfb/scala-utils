import com.fasterxml.jackson.annotation.JsonCreator
import no.uit.sfb.scalautils.yaml.Yaml
import org.scalatest.funspec.AnyFunSpec
import org.scalatest.matchers.should.Matchers

class YamlTest extends AnyFunSpec with Matchers {
  val obj = A(1)
  val obj2 = A(11, ab = B(22, Seq()), aopt = Some("opt"))
  describe(classOf[YamlTest].getName) {
    it("should serialize and parse to original") {
      val ser = Yaml.serialize[A](obj)
      val parsed = Yaml.parse[A](ser)
      parsed should be(obj)
    }
    /*it("should not serialize None") {
      val ser = Yaml.serialize[A](obj)
      ser.contains("aopt") should be(false)
    }*/
    it("should use default values when not present in input") {
      val inp = """
        |{
        |  "aa": 222,
        |  "ghost": "wohooooo"
        |}
      """.stripMargin
      println(Yaml.parse[A](inp))
      Yaml.parse[A](inp) should be(A(222))
    }
  }
}

case class A(aa: Int, ab: B = B(2), aopt: Option[String] = None) {
  @JsonCreator
  def this() = this(0)
}
case class B(ba: Int, bb: Seq[C] = Seq(C(), C("c1"))) {
  @JsonCreator
  def this() = this(0)
}
case class C(ca: String = "def\"[ault{C]a}-")
